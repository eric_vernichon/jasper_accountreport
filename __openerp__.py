# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Jasper Server Account reports ',
    'version': '105',
    'category': 'Impression',
    'description': """This module used JasperServer For Reports.

    Balance des Soldes
    Balance des soldes des comptes de tiers
    Grand livre avec contrepartie
    Grand livre des tiers
    Grand Livre

    """,
    'author': 'FIEF Management <evernichon@fiefmanage.ch>',
    'website': '',
    'depends': ['base', 'account', 'jasper_server'],
    'init_xml': [],
    'update_xml': ['security/jasper_accountreport_security.xml'
                   , 'security/ir.model.access.csv'
                   , 'jasper_accountreport_view.xml',
    ],
    'demo_xml': [],
    'installable': True,
    
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
