# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


from osv import osv, fields
from tools.translate import _


class uni_analytical_partners_ledger(osv.osv_memory):
    _name = 'uni.analytical.partners.ledger'

    _columns = {
        'partner_type': fields.selection([('client', 'Client'), ('supplier', 'Supplier')], 'Partner Type'),
        'period_selection_type': fields.selection(
            [('current', 'The current financial year up to and including the current period'),
             ('year', 'A specific financial year'), ('specific', 'Specific periods')], 'Select periods based on',
            required=True),
        'period_start_id': fields.many2one('account.period', 'Starting Accounting Period',
                                           domain=[('special', '=', False)]),
        'period_end_id': fields.many2one('account.period', 'Ending Accounting Period', domain=[('id', '<', 0)]),
        'financial_year_id': fields.many2one('account.fiscalyear', 'Financial Year'),
        'filter_analytical_accounts': fields.boolean('Filter Analytical Accounts'),
        'analytical_account_ids': fields.many2many('account.analytic.account', '', '', '', 'Analytical Accounts'),
        'filter_general_accounts': fields.boolean('Filter General Accounts', readonly=True),
        'general_account_ids': fields.many2many('account.account', '', '', '', 'Accounts', domain="[('id', '<', 0)]"),
        'filter_partners': fields.boolean('Filter Partners'),
        'partner_ids': fields.many2many('res.partner', '', '', '', 'Partners', domain=[('id', '<', 0)]),
        'group_partners': fields.boolean('Group Partners'),
        'format': fields.selection([('XLS', 'Excel'), ('PDF', 'PDF')], 'Output Format')
    }

    _defaults = {
        'partner_type': lambda *a: 'client',
        'period_selection_type': lambda *a: 'current',
        'filter_general_accounts': lambda *a: 'True',
        'format': lambda *a: 'PDF'
    }

    def onchange_partnertype(self, cr, uid, ids, partner_type):
        res = {}
        ids = []

        if partner_type == 'client':
            ids = [x for x in self.pool.get('account.account').search(cr, uid, ['|', ('type', '=', 'receivable'),
                                                                                      ('type', '=', 'receivables')])]
            domain = {'general_account_ids': ['|', ('type', '=', 'receivable'), ('type', '=', 'receivables')],
                      'partner_ids': [('customer', '=', 'true')]}
        else:
            ids = [x for x in self.pool.get('account.account').search(cr, uid, ['|', ('type', '=', 'payable'),
                                                                                      ('type', '=', 'payables')])]
            domain = {'general_account_ids': ['|', ('type', '=', 'payable'), ('type', '=', 'payables')],
                      'partner_ids': [('supplier', '=', 'true')]}
        values = {'general_account_ids': ids}
        res['domain'] = domain
        res['value'] = values
        return res

    def setup_period_selection(self, cr, uid, ids, period_selection_type, period_start_id, period_end_id):
        res = {}
        #if nothing was selected as a period selection type we just exit
        if not period_selection_type:
            return res

        if period_selection_type == 'specific':
            res = {}
            #we reset the selected values for start and end period
            if not period_start_id:
                values = {'period_start_id': 0, 'period_end_id': 0}
                domain = {'period_end_id': [('id', '<', 0)]}
                res['value'] = values
                res['domain'] = domain
            else:
                res = self.setup_period_end(cr, uid, ids, period_start_id, period_end_id)
        return res

    def setup_period_end(self, cr, uid, ids, period_start_id, period_end_id):
        res = {}
        period_obj = self.pool.get('account.period')
        period_start = period_obj.read(cr, uid, period_start_id)
        if not period_start:
            warning = {
            'Error retrieving starting accounting period': 'Starting accounting period with id' + period_start_id + 'could not be found.'}
            res['warning'] = warning
            return res
        #in a balance_sheet report the end period must be of the same fiscal year as starting period
        #we retrieve the id of the fiscal year of the selection
        start_date = period_start['date_start']
        domain = {'period_end_id': [('date_start', '>=', start_date), ('special', '=', False)]}

        if period_end_id:
            period_end = period_obj.read(cr, uid, period_end_id, ['date_start'])
            if period_end['date_start'] > period_start['date_stop']:
                #in this case we need to clear the period end
                res['value'] = {'period_end_id': 0}

        res['domain'] = domain
        return res

    def launch_report(self, cr, uid, ids, context):

        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr, uid, [])[0]

        #we gather the info from the wizard
        content_wiz = self.read(cr, uid, ids)[0]

        #the report is different whether we want to have currency detail or not
        if content_wiz['format'] == 'XLS':
            reportname = '/Reports/' + cr.dbname + '/analytical_partners_ledger_excel'
        else:
            reportname = '/Reports/' + cr.dbname + '/analytical_partners_ledger'

        is_partner_type_client = True
        if content_wiz['partner_type'] == 'supplier':
            is_partner_type_client = False

        #we go through the information that we need to collect no matter what report we are running
        #the first one is the financial periods
        fiscalyear_id = -1
        fiscalyear_obj = self.pool.get('account.fiscalyear')
        #User has three ways to specify financial periods to report
        #1.Specific period selection

        #the easiest case is a full fiscal exercise
        period_obj = self.pool.get('account.period')
        if content_wiz['period_selection_type'] == 'year':
            #the user had to specify the user_id
            fiscalyear_id = content_wiz['financial_year_id']
            #we retrieve the periods
            periods = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear_id), ('special', '=', False)],
                                        order='date_stop')
            period_start_id = periods[0]
            period_end_id = periods[-1]
            period_ids = str(tuple(periods))
        else:
            #when the user wants the current exercise, the last period is the current one
            if content_wiz['period_selection_type'] == 'current':
                fiscalyear_id = fiscalyear_obj.find(cr, uid)
                #we retrieve the periods
                periods = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear_id), ('special', '=', False)],
                                            order='date_stop')
                period_start_id = periods[0]
                period_end_id = period_obj.find(cr, uid)[0]
                if not period_end_id:
                    raise osv.except_osv(_('Error retrieving current accounting period'),
                                         _('No accounting periods found'))
            else:
                #we retrieve the data for the selected periods
                period_start_id = content_wiz['period_start_id']
                period_end_id = content_wiz['period_end_id']

            #for both cases we have now defined the start period and end period
            #the case where period_start_id is equal to period_end_id is trivial
            if period_start_id == period_end_id:
                period_ids = "(%s)" % period_start_id
            else:
                #we search periods that have a start date higher than the start date of the period_start, 
                #and a stop date lower than the stop date of the period_end
                start_date = period_obj.read(cr, uid, period_start_id, ['date_start'])['date_start']
                end_date = period_obj.read(cr, uid, period_end_id, ['date_stop'])['date_stop']
                periods = period_obj.search(cr, uid, [('date_start', '>=', start_date), ('date_stop', '<=', end_date),
                                                      ('special', '=', False)])
                #whatever the selection we change the list to a string
                period_ids = str(tuple(periods))

                #we retrieve the data for the analytical accounts
        analytical_account_ids = ""
        filters_label = ""
        if content_wiz['filter_analytical_accounts']:
            length_ids = len(content_wiz['analytical_account_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    analytical_account_ids = str(tuple(content_wiz['analytical_account_ids']))
                else:
                    analytical_account_ids = "(%s)" % content_wiz['analytical_account_ids'][0]

            analytical_accounts = self.pool.get('account.analytic.account').read(cr, uid,
                                                                                 content_wiz['analytical_account_ids'],
                                                                                 ['code'])
            analytical_account_labels = ""
            for i in range(0, length_ids):
                if i == 0:
                    if length_ids > 1:
                        analytical_account_labels = _('Accounts ')
                    else:
                        analytical_account_labels = _('Account ')
                else:
                    analytical_account_labels += ', '
                if i < 5:
                    analytical_account_labels += analytical_accounts[i]['code']
                else:
                    analytical_account_labels += _(' and others')
                    break
            filters_label = filters_label + analytical_account_labels + '\n'

        #we retrieve the data for the general accounts    
        general_account_ids = ""
        length_ids = len(content_wiz['general_account_ids'])
        if length_ids > 0:
            if length_ids > 1:
                general_account_ids = str(tuple(content_wiz['general_account_ids']))
            else:
                general_account_ids = "(%s)" % content_wiz['general_account_ids'][0]

        general_accounts = self.pool.get('account.account').read(cr, uid, content_wiz['general_account_ids'], ['code'])
        general_account_labels = ""
        for i in range(0, length_ids):
            if i == 0:
                if length_ids > 1:
                    general_account_labels = _('Accounts ')
                else:
                    general_account_labels = _('Account ')
            else:
                general_account_labels += ', '
            if i < 5:
                general_account_labels += general_accounts[i]['code']
            else:
                general_account_labels += _(' and others')
                break
        filters_label = filters_label + general_account_labels + '\n'

        #we retrieve the data for the partners  
        #if we send an empty string the report will show all partners  
        partner_ids = ""
        partner_labels = ""
        if content_wiz['filter_partners']:
            length_ids = len(content_wiz['partner_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    partner_ids = str(tuple(content_wiz['partner_ids']))
                else:
                    partner_ids = "(%s)" % content_wiz['partner_ids'][0]

                partners = self.pool.get('res.partner').read(cr, uid, content_wiz['partner_ids'], ['name'])
                for i in range(0, length_ids):
                    if i == 0:
                        if length_ids > 1:
                            partner_labels = _('Partners ')
                        else:
                            partner_labels = _('Partner ')
                    else:
                        partner_labels += ', '
                    if i < 5:
                        partner_labels += partners[i]['name']
                    else:
                        partner_labels += _(' and others')
                        break
            else:
                partner_labels = _('All partners taken into account')
        else:
            partner_labels = _('All partners taken into account')

        filters_label += (partner_labels + '\n')

        group_partners = False
        if content_wiz['group_partners'] == 1:
            group_partners = True

        data = {'form': {}}
        data['form']['params'] = [content_wiz['format'], reportname, 'period_start_id', 'period_stop_id',
                                  'is_partner_type_client', 'company_id', 'analytical_account_ids',
                                  'general_account_ids', 'period_ids', 'filters_label', 'partner_ids', 'group_partners']
        data['form']['period_start_id'] = period_start_id
        data['form']['period_stop_id'] = period_end_id
        data['form']['is_partner_type_client'] = is_partner_type_client
        data['form']['company_id'] = comp_id
        data['form']['analytical_account_ids'] = analytical_account_ids
        data['form']['general_account_ids'] = general_account_ids
        data['form']['period_ids'] = period_ids
        data['form']['filters_label'] = filters_label
        data['form']['partner_ids'] = partner_ids
        data['form']['group_partners'] = group_partners
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'print.jasper.pdf',
            'datas': data
        }


uni_analytical_partners_ledger()
