# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import pooler
import string
from tools.translate import _
from osv import osv, fields

class uni_account_analytic_balance(osv.osv_memory):
    
    _name = 'uni.account.analytic.balance'
    
    _columns = {
         'date_start': fields.date('From', required=True),
         'date_end': fields.date('To', required=True),
         'zero_balance': fields.boolean('Display Accounts With Zero Credit And Debit'),
         'filter_analytic_accounts': fields.boolean('Filter Analytic Accounts'),
         'analytic_account_ids': fields.many2many('account.analytic.account', '', '', '','Analytic Accounts', domain=[('type', '!=', 'view')]),
         'filter_general_accounts': fields.boolean('Filter General Accounts'),
         'general_account_ids': fields.many2many('account.account', '', '', '','General Accounts', domain=[('type', '!=', 'view')]),
         'hierarchy': fields.boolean('Display Account Hierarchy'),
         'format':fields.selection([('XLS','Excel'),('PDF','PDF')], 'Output Format')       
    }
    
    _defaults = {
         'format': lambda *a: 'PDF'   
    }
    
    def setup_date_end(self, cr, uid, ids, date_start, date_end):
        res = {}
        domain = {}
        values = {}
            
        if date_start != False and date_end != False:
            if date_start > date_end:
                warning = {_('Error with dates specified'): _('To date must be greater than From date')}
                res['warning'] = warning
                res['value'] = {'date_end': False}
                return res
        return res

    def onchange_hierarchy(self, cr, uid, ids, hierarchy):
        res={}
        if hierarchy:
            values = {'zero_balance':True}
            res['value']=values
        return res

    def launch_report(self, cr, uid, ids, context=None):
        
        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr,uid,[])[0]
        
        #we gather information from the wizard
        content_wiz = self.read(cr,uid,ids)[0]
        
        #first we figure out which report to display, based on the requested parameters
        reportname = '/Reports/'+cr.dbname+'/analytic_balance' 
        if content_wiz['format'] == 'XLS':
            if content_wiz['hierarchy']:
                reportname = '/Reports/'+cr.dbname+'/analytic_balance_hierarchy_excel'
            else:    
                reportname = '/Reports/'+cr.dbname+'/analytic_balance_excel'   
        else:
            if content_wiz['hierarchy']:
                reportname = '/Reports/'+cr.dbname+'/analytic_balance_hierarchy'
        
        #does the user want account with credit and debits = 0 to show on the report?
        print_zeroes = False
        if content_wiz['zero_balance']:
            print_zeroes = True
            
        data = {}
        data['form']={}
        data['form']['params']=[content_wiz['format'],reportname, 'date_start', 'date_stop', 'company_id', 'print_zeroes']
        data['form']['date_start'] = content_wiz['date_start']
        data['form']['date_stop'] = content_wiz['date_end']
        data['form']['company_id'] = comp_id
        data['form']['print_zeroes'] = print_zeroes
                  
        return {
                'type': 'ir.actions.report.xml',
                'report_name': 'print.jasper.pdf',
                'datas': data
        }
    
uni_account_analytic_balance()
