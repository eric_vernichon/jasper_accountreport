# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


import pooler
from osv import osv, fields
from tools.translate import _


class general_ledger(osv.osv_memory):
    
    _name = 'jasper_accountreport.general_ledger'
    
    _columns = {
        'period_selection_type': fields.selection([('current','The current financial year up to and including the current period'),\
                                             ('year', 'A specific financial year'), ('specific', 'Specific periods')], 'Select periods based on', required=True),
        'period_start_id': fields.many2one('account.period', 'Starting Accounting Period'),
        'period_end_id': fields.many2one('account.period', 'Ending Accounting Period'),
        'financial_year_id': fields.many2one('account.fiscalyear', 'Financial Year'),
        'filter_accounts': fields.boolean('Filter Accounts'),
        'account_ids': fields.many2many('account.account', '', '', '','Accounts', domain=[('type', '!=', 'view')]),
        'draft': fields.boolean('Include draft movements'),
        'include_ccy': fields.boolean('Include currency information'),
        'filter_ccy': fields.boolean('Filter Currencies'),
        'currency_ids':fields.many2many('res.currency', '', '', '', 'Currencies'),
        'format':fields.selection([('XLS','Excel'),('PDF','PDF')], 'Output Format')     
    }
    
    _defaults = {
        'period_selection_type': lambda *a: 'current',
        'format': lambda *a: 'PDF'
    }

    def setup_period_selection(self, cr, uid, ids, period_selection_type):
        res = {}
        #if nothing was selected as a period selection type we just exit
        if not period_selection_type: 
            return res
        
        if period_selection_type == 'specific' :
            res = {}
            #we reset the selected values for start and end period
            values = {'period_start_id': 0, 'period_end_id': 0}
            domain = {'period_end_id':[('id','<', 0)]}
            res['value'] = values
            res['domain'] = domain
        return res
        
    def setup_period_end(self, cr, uid, ids, period_start_id):
        res = {}
        period_obj = self.pool.get('account.period')
        period_start = period_obj.read(cr, uid, period_start_id)
        if not period_start:
            warning = {'Error retrieving starting accounting period': 'Starting accounting period with id' + period_start_id + 'could not be found.'}
            res['warning'] = warning
            return res
        #in a balance_sheet report the end period must be of the same fiscal year as starting period
        #we retrieve the id of the fiscal year of the selection
        values = {'period_end_id': 0}
        start_date = period_start['date_start']
        domain = {'period_end_id':[('date_start','>=', start_date)]}
        res['value'] = values
        res['domain'] = domain
        return res
    
    def onchange_includeccy(self, cr, uid, ids, include_ccy):
        res={}
        if include_ccy:
            values = {'filter_ccy': False}
            res['value']=values
        return res

    def launch_report(self, cr, uid, ids, context):
        
        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr,uid,[])[0]
        
        #we gather the info from the wizard
        content_wiz = self.pool.get('jasper_accountreport.general_ledger').read(cr,uid,ids)[0]
        
        #the report is different whether we want to have currency detail or not
        if content_wiz['include_ccy']:
            if content_wiz['format'] == 'XLS':
                reportname = '/Reports/'+cr.dbname+'/general_ledger_w_currency_details_excel'
            else:
                reportname = '/Reports/'+cr.dbname+'/general_ledger_w_currency_details'
        else:
            if content_wiz['format'] == 'XLS':
                reportname = '/Reports/'+cr.dbname+'/general_ledger_excel'
            else:
                reportname = '/Reports/'+cr.dbname+'/general_ledger'
            
        #we go through the information that we need to collect no matter what report we are running
        #the first one is the financial periods
        start_date = None
        stop_date = None
        fiscalyear_id = -1
        
        #User has three ways to specify financial periods to report
        #1.Specific period selection
        
        if content_wiz['period_selection_type'] == 'specific':
            #we retrieve the data for the selected periods
            period_obj = self.pool.get('account.period')
            period_start = period_obj.read(cr, uid, content_wiz['period_start_id'][0])
            start_date = period_start['date_start']
            period_end = period_obj.read(cr, uid, content_wiz['period_end_id'][0])
            stop_date = period_end['date_stop']
        else:
            fiscalyear_obj = self.pool.get('account.fiscalyear')
        #2. The current financial year, up to the current period...    
            #...should it be the current year...
            if content_wiz['period_selection_type'] == 'current':
                fiscalyear_id = fiscalyear_obj.find(cr, uid)
                fiscalyear = fiscalyear_obj.read(cr, uid, fiscalyear_id)
                start_date = fiscalyear['date_start']
                #we get the current period
                current_period_obj = self.pool.get('account.period')
                current_period_id = current_period_obj.find(cr, uid)
                if not current_period_id:
                    raise osv.except_osv(_('Error retrieving current accounting period'), _('No accounting periods found'))
                current_period = current_period_obj.read(cr, uid, current_period_id[0])
                stop_date = current_period['date_stop']
            #...or a specific year selected by the user
            else:
        #3. A complete financial exercise selected by the user 
                fiscalyear_id = content_wiz['financial_year_id']
                #we retrieve the financial year object
                fiscalyear = fiscalyear_obj.read(cr, uid, fiscalyear_id)
                start_date = fiscalyear[0]['date_start']
                stop_date= fiscalyear[0]['date_stop']     
            
        #we retrieve the data for the accounts
        account_ids = ""
        filters_label = ""
        if content_wiz['filter_accounts']:
            length_ids = len(content_wiz['account_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    account_ids = str(tuple(content_wiz['account_ids']))
                else:
                    account_ids = "(%s)" % content_wiz['account_ids'][0]

        #we retrieve the data for the currencies
        #if we send an empty string the report will show all partners  
        currency_ids = ""
        if content_wiz['filter_ccy']:
            length_ids = len(content_wiz['currency_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    currency_ids = str(tuple(content_wiz['currency_ids']))
                else:
                    currency_ids = "(%s)" % content_wiz['currency_ids'][0]
        
        include_drafts = content_wiz['draft']
        
        data = {}
        data['form']={}
        data['form']['params']=[content_wiz['format'],reportname, 'date_start', 'date_stop', 'company_id', 'account_ids', 'filters_label', 'include_drafts', 'currency_ids']
        data['form']['date_start'] = start_date
        data['form']['date_stop'] = stop_date
        data['form']['company_id'] = comp_id
        data['form']['account_ids'] = account_ids
        data['form']['filters_label'] = filters_label
        data['form']['include_drafts'] = include_drafts
        data['form']['currency_ids'] = currency_ids

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'print.jasper.pdf',
            'datas': data
        }
        
general_ledger()
