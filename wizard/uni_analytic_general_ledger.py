# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2013 Fief Developement All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


from osv import osv, fields
from tools.translate import _


class uni_analytic_general_ledger(osv.osv_memory):
    
    _name = 'uni.analytic.general.ledger'
    
    _columns = {
        'date_start': fields.date('From', required=True),
        'date_end': fields.date('To', required=True),
        'filter_accounts': fields.boolean('Filter Accounts'),
        'account_ids': fields.many2many('account.analytic.account', '', '', '',
                                        'Accounts', domain=[('type', '!=', 'view')]),
        'format': fields.selection([('XLS', 'Excel'), ('PDF', 'PDF')], 'Output Format')
    }
    
    _defaults = {
        'format': lambda *a: 'PDF'
    }

    def setup_date_end(self, cr, uid, ids, date_start, date_end):
        res = {}
            
        if date_start != False and date_end != False:
            if date_start > date_end:
                warning = {_('Error with dates specified'): _('To date must be greater than From date')}
                res['warning'] = warning
                res['value'] = {'date_end': False}
                return res
        return res

    def launch_report(self, cr, uid, ids, context):
        
        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr, uid, [])[0]
        
        #we gather the info from the wizard
        content_wiz = self.read(cr, uid, ids)[0]
        
        #the report is different depending on the format
        if content_wiz['format'] == 'XLS':
            reportname = '/Reports/' + cr.dbname + '/analytic_general_ledger_excel'
        else:
            reportname = '/Reports/' + cr.dbname + '/analytic_general_ledger'
            
        #we retrieve the data for the accounts    
        account_ids = ""
        length_ids = len(content_wiz['account_ids'])
        if length_ids > 0:
            if length_ids > 1:
                account_ids = str(tuple(content_wiz['account_ids']))
            else:
                account_ids = "(%s)" % content_wiz['account_ids'][0]
              
        account_labels = ""
        if content_wiz['filter_accounts']:
            length_ids = len(content_wiz['account_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    account_ids = str(tuple(content_wiz['account_ids']))
                else:
                    account_ids = "(%s)" % content_wiz['account_ids'][0]
                    
                accounts = self.pool.get('account.analytic.account').read(cr, uid, content_wiz['account_ids'], ['code'])
                for i in range(0, length_ids):
                    if i == 0:
                        if length_ids > 1:
                            account_labels = _('Accounts ')
                        else:
                            account_labels = _('Account ')
                    else:
                        account_labels += ', '
                    if i < 5:
                        account_labels += accounts[i]['code']
                    else:
                        account_labels += _(' and others')
                        break
            else:
                account_labels = _('All accounts taken into account')
        else:
            account_labels = _('All accounts taken into account')      
        account_labels += '\n'
        
        data = {'form': {}}
        data['form']['params'] = [content_wiz['format'], reportname, 'date_start', 'date_stop', 'company_id',
                                  'account_ids', 'filters_label']
        data['form']['date_start'] = content_wiz['date_start']
        data['form']['date_stop'] = content_wiz['date_end']
        data['form']['company_id'] = comp_id
        data['form']['account_ids'] = account_ids
        data['form']['filters_label'] = account_labels
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'print.jasper.pdf',
            'datas': data
        }
