# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


from osv import osv, fields
from tools.translate import _


class partners_ledger(osv.osv_memory):
    _name = 'jasper_accountreport.partners_ledger'

    _columns = {
        'partner_type': fields.selection([('client', 'Client'), ('supplier', 'Supplier')], 'Partner Type'),
        'period_selection_type': fields.selection(
            [('current', 'The current financial year up to and including the current period'),
             ('year', 'A specific financial year'), ('specific', 'Specific periods')], 'Select periods based on',
            required=True),
        'period_start_id': fields.many2one('account.period', 'Starting Accounting Period'),
        'period_end_id': fields.many2one('account.period', 'Ending Accounting Period'),
        'financial_year_id': fields.many2one('account.fiscalyear', 'Financial Year'),
        'filter_accounts': fields.boolean('Filter Accounts', readonly=True),
        'account_ids': fields.many2many('account.account', '', '', ''),
        'filter_partners': fields.boolean('Filter Partners'),
        'partner_ids': fields.many2many('res.partner', '', '', '', 'Partners', domain="[('id', '<', 0)]"),
        'draft': fields.boolean('Include draft movements'),
        'include_ccy': fields.boolean('Include currency information'),
        'filter_ccy': fields.boolean('Filter Currencies'),
        'currency_ids': fields.many2many('res.currency', '', '', '', 'Currencies'),
        'format': fields.selection([('XLS', 'Excel'), ('PDF', 'PDF')], 'Output Format')
    }

    _defaults = {
        'partner_type': lambda *a: 'client',
        'period_selection_type': lambda *a: 'current',
        'filter_accounts': lambda *a: 'True',
        'format': lambda *a: 'PDF'
    }

    def onchange_partnertype(self, cr, uid, wizard_id, partner_type):
        res = {}

        if partner_type == 'client':
            ids = [x for x in self.pool.get('account.account').search(cr, uid, ['|', ('type', '=', 'receivable'),
                                                                                     ('type', '=', 'receivables')])]
            domain = {'account_ids': ['|', ('type', '=', 'receivable'), ('type', '=', 'receivables')],
                      'partner_ids': [('customer', '=', True)]}
        else:
            ids = [x for x in self.pool.get('account.account').search(cr, uid, ['|', ('type', '=', 'payable'),
                                                                                     ('type', '=', 'payables')])]
            domain = {'account_ids': ['|', ('type', '=', 'payable'), ('type', '=', 'payables')],
                      'partner_ids': [('supplier', '=', True)]}
        values = {'account_ids': ids}
        res['domain'] = domain
        res['value'] = values
        return res

    def setup_period_selection(self, cr, uid, ids, period_selection_type):
        res = {}
        #if nothing was selected as a period selection type we just exit
        if not period_selection_type:
            return res

        if period_selection_type == 'specific':
            res = {}
            #we reset the selected values for start and end period
            values = {'period_start_id': 0, 'period_end_id': 0}
            domain = {'period_end_id': [('id', '<', 0)]}
            res['value'] = values
            res['domain'] = domain
        return res

    def setup_period_end(self, cr, uid, ids, period_start_id):
        res = {}
        period_obj = self.pool.get('account.period')
        period_start = period_obj.read(cr, uid, period_start_id)
        if not period_start:
            warning = {
            'Error retrieving starting accounting period': 'Starting accounting period with id' + period_start_id + 'could not be found.'}
            res['warning'] = warning
            return res
        #in a balance_sheet report the end period must be of the same fiscal year as starting period
        #we retrieve the id of the fiscal year of the selection
        values = {'period_end_id': 0}
        start_date = period_start['date_start']
        domain = {'period_end_id': [('date_start', '>=', start_date)]}
        res['value'] = values
        res['domain'] = domain
        return res

    def onchange_includeccy(self, cr, uid, ids, include_ccy):
        res = {}
        if include_ccy:
            values = {'filter_ccy': False}
            res['value'] = values
        return res

    def launch_report(self, cr, uid, ids, context):

        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr, uid, [])[0]

        #we gather the info from the wizard
        content_wiz = self.read(cr, uid, ids)[0]
        print "content_wiz" , content_wiz
        #the report is different whether we want to have currency detail or not
        if content_wiz['include_ccy']:
            if content_wiz['format'] == 'XLS':
                reportname = '/Reports/' + cr.dbname + '/partners_ledger_w_currency_details_excel'
            else:
                reportname = '/Reports/' + cr.dbname + '/partners_ledger_w_currency_details'
        else:
            if content_wiz['format'] == 'XLS':
                reportname = '/Reports/' + cr.dbname + '/partners_ledger_excel'
            else:
                reportname = '/Reports/' + cr.dbname + '/partners_ledger'

        is_partner_type_client = True
        if content_wiz['partner_type'] == 'supplier':
            is_partner_type_client = False

        #we go through the information that we need to collect no matter what report we are running
        #the first one is the financial periods
        fiscalyear_id = -1
        fiscalyear_obj = self.pool.get('account.fiscalyear')
        #User has three ways to specify financial periods to report
        #1.Specific period selection

        #the easiest case is a full fiscal exercise
        period_obj = self.pool.get('account.period')
        #the easiest case is a full fiscal exercise
        if content_wiz['period_selection_type'] == 'year':
            #the user had to specify the user_id
            fiscalyear_id = content_wiz['financial_year_id']
            #we retrieve the periods
            periods = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear_id)], order='date_stop, special')
            period_start_id = periods[0]
            period_end_id = periods[-1]
            period_ids = str(tuple(periods))
        else:
            #when the user wants the current exercise, the last period is the current one
            if content_wiz['period_selection_type'] == 'current':
                fiscalyear_id = fiscalyear_obj.find(cr, uid)
                #we retrieve the periods
                periods = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear_id)],
                                            order='date_stop, special')
                period_start_id = periods[0]
                period_end_id = period_obj.find(cr, uid)[0]
                if not period_end_id:
                    raise osv.except_osv(_('Error retrieving current accounting period'),
                                         _('No accounting periods found'))
            else:
                #we retrieve the data for the selected periods
                period_start_id = content_wiz['period_start_id']
                period_end_id = content_wiz['period_end_id']

            #for both cases we have now defined the start period and end period
            #the case where period_start_id is equal to period_end_id is trivial
            if period_start_id == period_end_id:
                period_ids = "(%s)" % period_start_id
            else:
                #we loop through them backwards until we find the last one to include
                start_date = period_obj.read(cr, uid, period_start_id, ['date_start'])['date_start']
                end_date = period_obj.read(cr, uid, period_end_id, ['date_stop'])['date_stop']
                periods = period_obj.search(cr, uid, [('date_start', '>=', start_date), ('date_stop', '<=', end_date),
                                                      ('special', '=', False)])
                #whatever the selection we change the list to a string
                period_ids = str(tuple(periods))

        #we retrieve the data for the accounts    
        account_ids = ""
        length_ids = len(content_wiz['account_ids'])
        if length_ids > 0:
            if length_ids > 1:
                account_ids = str(tuple(content_wiz['account_ids']))
            else:
                account_ids = "(%s)" % content_wiz['account_ids'][0]

        #we retrieve the data for the partners  
        #if we send an empty string the report will show all partners  
        partner_ids = ""
        if content_wiz['filter_partners']:
            length_ids = len(content_wiz['partner_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    partner_ids = str(tuple(content_wiz['partner_ids']))
                else:
                    partner_ids = "(%s)" % content_wiz['partner_ids'][0]

        #we retrieve the data for the currencies
        #if we send an empty string the report will show all partners  
        currency_ids = ""
        currency_labels = ""
        if content_wiz['filter_ccy']:
            length_ids = len(content_wiz['currency_ids'])
            if length_ids > 0:
                if length_ids > 1:
                    currency_ids = str(tuple(content_wiz['currency_ids']))
                else:
                    currency_ids = "(%s)" % content_wiz['currency_ids'][0]


        include_drafts = False
        if content_wiz['draft'] == 1:
            include_drafts = True

        data = {'form': {}}
        data['form']['params'] = [content_wiz['format'], reportname, 'period_start_id', 'period_stop_id',
                                  'is_partner_type_client', 'company_id', 'account_ids', 'period_ids',
                                  'partner_ids', 'include_drafts', 'currency_ids']
        data['form']['period_start_id'] = period_start_id
        data['form']['period_stop_id'] = period_end_id
        data['form']['is_partner_type_client'] = is_partner_type_client
        data['form']['company_id'] = comp_id
        data['form']['account_ids'] = account_ids
        data['form']['period_ids'] = period_ids
        data['form']['partner_ids'] = partner_ids
        data['form']['include_drafts'] = include_drafts
        data['form']['currency_ids'] = currency_ids
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'print.jasper.pdf',
            'datas': data
        }


partners_ledger()
