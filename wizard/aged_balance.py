# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


from osv import osv, fields
from datetime import datetime


class aged_balance(osv.osv_memory):
    _name = 'jasper_accountreport.aged_balance'

    _columns = {
        'date_state': fields.date('As Of Date'),
        'report_type': fields.selection([('clients', 'What clients owe'), ('suppliers', 'What suppliers are owed')],
                                        'Information to Display'),
        'display_details': fields.boolean('Display Debit / Credit Details'),
        'zero_due': fields.boolean('Display Partners Without Amounts'),
        'format': fields.selection([('XLS', 'Excel'), ('PDF', 'PDF')], 'Output Format'),
        'account_ids': fields.many2many('account.account', '', '', '', 'Accounts', domain="[('id', '<', 0)]")
    }

    _defaults = {
        'date_state': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'format': lambda *a: 'PDF',
        'report_type': lambda *a: 'clients'
    }

    def get_accounts(self, cr, uid, ids, report_type):
        ids = []
        if report_type == 'clients':
            ids = [x for x in self.pool.get('account.account').search(cr, uid, [('type', '=', 'receivable')])]
            domain = {'account_ids': [('type', '=', 'receivable')]}
        else:
            ids = [x for x in self.pool.get('account.account').search(cr, uid, [('type', '=', 'payable')])]
            domain = {'account_ids': [('type', '=', 'payable')]}
        values = {'account_ids': ids}
        res = {'domain': domain, 'value': values}
        return res

    def launch_report(self, cr, uid, ids, context=None):

        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr, uid, [])[0]

        #we gather the info from the wizard
        content_wiz = self.pool.get('jasper_accountreport.aged_balance').read(cr, uid, ids)[0]

        if content_wiz['display_details']:
            if content_wiz['format'] == 'XLS':
                reportname = '/Reports/' + cr.dbname + '/aged_balance_detailed_excel'
            else:
                reportname = '/Reports/' + cr.dbname + '/aged_balance_detailed'
        else:
            if content_wiz['format'] == 'XLS':
                reportname = '/Reports/' + cr.dbname + '/aged_balance_excel'
            else:
                reportname = '/Reports/' + cr.dbname + '/aged_balance'


        is_partner_type_client = True
        if content_wiz['report_type'] == 'suppliers':
            is_partner_type_client = False

        account_ids = ""
        length_ids = len(content_wiz['account_ids'])
        if length_ids > 0:
            if length_ids > 1:
                account_ids = str(tuple(content_wiz['account_ids']))
            else:
                account_ids = "(%s)" % content_wiz['account_ids'][0]

        account_labels = ""
        accounts = self.pool.get('account.account').read(cr, uid, content_wiz['account_ids'], ['code'])
        for i in range(0, length_ids):
            if i != 0:
                account_labels += ", "
            account_labels += accounts[i]['code']

        print_zeroes = False
        if content_wiz['zero_due']:
            print_zeroes = True

        print "Lancement "
        data = {'form': {}}
        data['form']['params'] = [content_wiz['format'], reportname, 'date_state', 'is_partner_type_client',
                                  'company_id', 'print_zeroes', 'account_ids', 'account_labels']
        data['form']['date_state'] = content_wiz['date_state']
        data['form']['is_partner_type_client'] = is_partner_type_client
        data['form']['company_id'] = comp_id
        data['form']['account_ids'] = account_ids
        data['form']['print_zeroes'] = print_zeroes
        data['form']['account_labels'] = account_labels

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'print.jasper.pdf',
            'datas': data
        }


aged_balance()
