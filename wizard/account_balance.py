# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2007 EVI All Rights Reserved.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


from tools.translate import _
from osv import osv, fields


class account_balance(osv.osv_memory):
    _name = 'jasper_accountreport.account_balance'

    def _selection_setup_for_additional_data(self, cr, uid, context=None):
        if context is None:
            context = {}
        res = [('none', _('No Additional Information')), ('currency', _('Currency Details'))]

        cr.execute("""SELECT * FROM ir_module_module
                    WHERE name = 'uni_budget'
                    AND state = 'installed'""")
        uni_budget_installed = cr.fetchall()
        if uni_budget_installed:
            #module_obj=self.pool.get('ir.module.module')
            #ids=module_obj.search(cr, uid, [('name', '=', 'uni_budget'), ('state', '=', 'installed')])
            #if len(ids)>0:
            res.append(('budget', _('Budget Details')))
        return res

    _columns = {
        'report_type': fields.selection([('complete', 'Balance Sheet And Income Statement'),
                                         ('balance_sheet', 'Balance Sheet'),
                                         ('income', 'Income Statement')], 'Report Type', required=True),
        'period_selection_type': fields.selection(
            [('current', 'The current financial year up to and including the current period'),
             ('year', 'A specific financial year'),
             ('specific', 'Specific periods')], 'Select periods based on', required=True),
        'period_start_id': fields.many2one('account.period', 'Starting Accounting Period'),
        'period_end_id': fields.many2one('account.period', 'Ending Accounting Period'),
        'financial_year_id': fields.many2one('account.fiscalyear', 'Financial Year'),
        'filter_analytic_accounts': fields.boolean('Filter Analytic Accounts'),
        'analytic_account_ids': fields.many2many('account.analytic.account', 'jasper_account_balance_analytic_rel',
                                                 'report_id', 'analytic_account_id', 'Analytic Accounts',
                                                 domain=[('type', '!=', 'view')]),
        'filter_general_accounts': fields.boolean('Filter General Accounts'),
        'general_account_ids': fields.many2many('account.account', 'jasper_account_balance_account_rel',
                                                'report_id', 'account_id', 'General Accounts',
                                                domain=[('type', '!=', 'view')]),
        'zero_balance': fields.boolean('Display Accounts With Zero Credit And Debit'),
        'subtotal': fields.boolean('Totals By Class'),
        'hierarchy': fields.boolean('Display Account Hierarchy', required=False),
        'additional_data': fields.selection(_selection_setup_for_additional_data, 'Additional Data To Include'),
        'format': fields.selection([('XLS', 'Excel'), ('PDF', 'PDF')], 'Output Format')
    }

    _defaults = {
        'report_type': lambda *a: 'complete',
        'period_selection_type': lambda *a: 'current',
        'additional_data': lambda *a: 'none',
        'format': lambda *a: 'PDF'
    }

    def setup_period_selection(self, cr, uid, ids, report_type, period_selection_type):
        res = {}
        #if nothing was selected as a period selection type we just exit
        if not period_selection_type:
            return res

        if period_selection_type == 'specific':
            res = {}
            #we reset the selected values for start and end period
            values = {'period_start_id': 0, 'period_end_id': 0}
            if report_type == 'income':
                domain = {'period_start_id': [('special', '=', False)], 'period_end_id': [('id', '<', 0)]}
            else:
                domain = {'period_start_id': ['&', ('special', '=', True), ('code', 'like', "OU%")],
                          'period_end_id': [('id', '<', 0)]}
            res['value'] = values
            res['domain'] = domain
        return res

    def setup_period_end(self, cr, uid, ids, report_type, period_start_id):
        res = {}
        period_obj = self.pool.get('account.period')
        period_start = period_obj.read(cr, uid, period_start_id)
        if not period_start:
            warning = {
                'Error retrieving starting accounting period': 'Starting accounting period with id' + period_start_id + 'could not be found.'}
            res['warning'] = warning
            return res
        #in a complete or balance_sheet report the end period must be of the same fiscal year as starting period
        #we retrieve the id of the fiscal year of the selection
        values = {'period_end_id': 0}
        if report_type != 'income':
            fiscalyear_id = period_start['fiscalyear_id'][0]
            domain = {'period_end_id': [('fiscalyear_id', '=', fiscalyear_id)]}
        #in an income statement the end period must come after the starting period
        else:
            start_date = period_start['date_start']
            domain = {'period_end_id': ['&', ('date_start', '>=', start_date), ('special', '=', False)]}
        res['value'] = values
        res['domain'] = domain
        return res

    def onchange_additionaldata(self, cr, uid, ids, additional_data):
        res = {}
        if additional_data == 'currency':
            values = {'hierarchy': False}
            res['value'] = values
        return res

    def onchange_hierarchy(self, cr, uid, ids, hierarchy):
        res = {}
        if hierarchy:
            values = {'subtotal': False}
            res['value'] = values
        return res

    def launch_report(self, cr, uid, ids, context=None):

        comp_obj = self.pool.get('res.company')
        comp_id = comp_obj.search(cr, uid, [])[0]

        #We gather the values from the wizard
        content_wiz = self.read(cr, uid, ids)[0]

                #first we figure out which report to display, based on the requested parameters
        reportname = '/Reports/'+cr.dbname+'/ab_no_details'
        if content_wiz['format'] == 'XLS':
            if content_wiz['hierarchy']:
                if content_wiz['additional_data'] == 'budget':
                    reportname = '/Reports/'+cr.dbname+'/ab_w_budget_details_hierarchy_excel'
                else:
                    reportname = '/Reports/'+cr.dbname+'/ab_no_details_hierarchy_excel'
            else:
                if content_wiz['additional_data'] == 'currency':
                    reportname = '/Reports/'+cr.dbname+'/ab_w_currency_details_excel'
                elif content_wiz['additional_data'] == 'budget':
                    reportname = '/Reports/'+cr.dbname+'/ab_w_budget_details_excel'
                else:
                    reportname = '/Reports/'+cr.dbname+'/ab_no_details_excel'
        else:
            if content_wiz['hierarchy']:
                if content_wiz['additional_data'] == 'budget':
                    reportname = '/Reports/'+cr.dbname+'/ab_w_budget_details_hierarchy'
                else:
                    reportname = '/Reports/'+cr.dbname+'/ab_no_details_hierarchy'
            else:
                if content_wiz['additional_data'] == 'currency':
                    reportname = '/Reports/'+cr.dbname+'/ab_w_currency_details'
                elif content_wiz['additional_data'] == 'budget':
                    reportname = '/Reports/'+cr.dbname+'/ab_w_budget_details'

        #the report type influences whether or not we take into account special periods or not
        special = content_wiz['report_type'] != 'income'

        #we go through the information that we need to collect no matter what report we are running
        #the first one is the financial periods
        fiscalyear_id = -1
        fiscalyear_obj = self.pool.get('account.fiscalyear')
        period_obj = self.pool.get('account.period')

        #User has three ways to specify financial periods to report
        #For each case we need to determine the boundary periods

        #The easiest case is a full fiscal exercise
        if content_wiz['period_selection_type'] == 'year':
            #the user had to specify the user_id
            fiscalyear_id = content_wiz['financial_year_id']
            #we retrieve the periods
            periods = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear_id),
                                                  ('special', '=', special)], order='date_stop')
            period_start_id = periods[0]
            period_end_id = periods[-1]
        else:
            #when the user wants the current exercise, the last period is the current one
            if content_wiz['period_selection_type'] == 'current':
                fiscalyear_id = fiscalyear_obj.find(cr, uid)
                #we retrieve the periods
                periods = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear_id),
                                                      ('special', '=', special)], order='date_stop')
                period_start_id = periods[0]
                period_end_id = period_obj.find(cr, uid)[0]
                if not period_end_id:
                    raise osv.except_osv(_('Error retrieving current accounting period'),
                                         _('No accounting periods found'))
            else:
                #we retrieve the data for the selected periods
                period_start_id = content_wiz['period_start_id'][0]
                period_end_id = content_wiz['period_end_id'][0]

        #Now that we have the start and end period, we can determine all the periods to take into account
        if period_start_id == period_end_id:
            #if start and end period are the same it's trivial
            period_ids = "(%s)" % period_start_id
        else:
            #If not we read all periods that are between the start and stop period
            start_date = period_obj.read(cr, uid, period_start_id, ['date_start'])['date_start']
            end_date = period_obj.read(cr, uid, period_end_id, ['date_stop'])['date_stop']
            # only the start and stop period can be special, in some circumstances.
            # Therefore all the periods in between cannot be special
            periods = period_obj.search(cr, uid, [('date_start','>=', start_date),('date_stop','<=',end_date), ('special','=',False)])
            #did we omit the start and stop by excluding specials?
            if period_start_id not in periods:
                periods.append(period_start_id)
            if period_end_id not in periods:
                periods.append(period_end_id)
            #whatever the selection we change the list to a string
            period_ids = str(tuple(periods))

        #does the user want account with credit and debits = 0 to show on the report?
        print_zeroes = False
        if content_wiz['zero_balance']:
            print_zeroes = True

        limit_to_accounts = 'none'
        if content_wiz['report_type'] == 'income':
            limit_to_accounts = 'income'
        elif content_wiz['report_type'] == 'balance_sheet':
            limit_to_accounts = 'balance_sheet'

        #we retrieve the data for the accounts
        analytic_account_ids = ""
        length_ids = len(content_wiz['analytic_account_ids'])
        if length_ids >0:
            if length_ids >1:
                analytic_account_ids = str(tuple(content_wiz['analytic_account_ids']))
            else:
                analytic_account_ids = "(%s)"%content_wiz['analytic_account_ids'][0]

        general_account_ids = ""
        length_ids = len(content_wiz['general_account_ids'])
        if length_ids >0:
            if length_ids >1:
                general_account_ids = str(tuple(content_wiz['general_account_ids']))
            else:
                general_account_ids = "(%s)"%content_wiz['general_account_ids'][0]

        data = {}
        data['form']={}
        data['form']['params']=[content_wiz['format'],reportname, 'period_start_id', 'period_stop_id', 'company_id', 'period_ids', 'print_zeroes', 'limit_to_accounts',
                                'analytic_account_ids','general_account_ids']
        data['form']['period_start_id'] = period_start_id
        data['form']['period_stop_id'] = period_end_id
        data['form']['company_id'] = comp_id
        data['form']['period_ids'] = period_ids
        data['form']['print_zeroes'] = print_zeroes
        data['form']['limit_to_accounts'] = limit_to_accounts
        data['form']['analytic_account_ids'] = analytic_account_ids
        data['form']['general_account_ids'] = general_account_ids

        if content_wiz['additional_data'] == 'budget':
            data['form']['params'].append('fiscalyear_id')
            data['form']['fiscalyear_id'] = fiscalyear_id

        #in non hierarchical reports, extra parameter to define if the user wants to display 
        #sub totals by class or not
        if content_wiz['hierarchy'] != 'yes':
            subtotal = False
            if content_wiz['subtotal'] == 1:
                subtotal = True
            data['form']['params'].append('subtotal')
            data['form']['subtotal'] = subtotal

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'print.jasper.pdf',
            'datas': data
        }

account_balance()
