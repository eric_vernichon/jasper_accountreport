<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ab_no_details_subreport" pageWidth="424" pageHeight="555" orientation="Landscape" columnWidth="424" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="d3f961f7-1f00-4600-8190-933e966887a7">
	<property name="ireport.zoom" value="4.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.jasperserver.url" value="http://aws-eu-1.uniforme.ch:18080/jasperserver/services/repository"/>
	<style name="encadre">
		<box rightPadding="6">
			<topPen lineWidth="0.75"/>
			<leftPen lineWidth="0.75"/>
			<bottomPen lineWidth="0.75"/>
			<rightPen lineWidth="0.75"/>
		</box>
	</style>
	<style name="AlternateDetailRowStyle" backcolor="#E1E1E1"/>
	<parameter name="company_id" class="java.lang.Integer"/>
	<parameter name="period_ids" class="java.lang.String">
		<defaultValueExpression><![CDATA["SELECT (SELECT code FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS devcompany,"
+" CASE WHEN t.code IN ('expense', 'income') THEN 1 ELSE 0 END AS account_type,"
+"       SUM(debit) AS debit,"
+"       SUM(credit) AS credit,"
+"       SUM(debit-credit) AS solde"
+" FROM public.account_move_line m"
+" INNER JOIN public.account_account a ON a.id = m.account_id"
+" INNER JOIN public.account_period p ON p.id = m.period_id"
+" INNER JOIN account_account_type t ON a.user_type = t.id"
+" LEFT JOIN public.res_currency res_currency ON res_currency.id = m.currency_id"
+" WHERE a.type != 'view'"
+" AND m.state != 'draft'"
+" AND t.code != 'special'"
+" AND p.id IN " + $P{period_ids}
+" GROUP BY account_type"
+" ORDER BY account_type"]]></defaultValueExpression>
	</parameter>
	<parameter name="query" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["SELECT (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS devcompany,"
+" CASE WHEN t.code IN ('expense', 'income') THEN 'Compte de résultat' ELSE 'Compte de bilan' END AS account_type,"
+"       SUM(debit) AS debit,"
+"       SUM(credit) AS credit,"
+"       SUM(debit-credit) AS solde"
+" FROM public.account_move_line m"
+" INNER JOIN public.account_account a ON a.id = m.account_id"
+" INNER JOIN public.account_period p ON p.id = m.period_id"
+" INNER JOIN account_account_type t ON a.user_type = t.id"
+" LEFT JOIN public.res_currency res_currency ON res_currency.id = m.currency_id"
+" WHERE a.type != 'view'"
+" AND m.state != 'draft'"
+" AND t.code != 'special'"
+" AND p.id IN " + $P{period_ids}
+" GROUP BY account_type"
+" ORDER BY account_type"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[$P!{query}]]>
	</queryString>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<field name="solde" class="java.math.BigDecimal"/>
	<field name="devcompany" class="java.lang.String"/>
	<field name="account_type" class="java.lang.String"/>
	<variable name="debit_sum" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<variable name="credit_sum" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<variable name="solde_sum" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{solde}]]></variableExpression>
	</variable>
	<background>
		<band/>
	</background>
	<title>
		<band height="25">
			<line>
				<reportElement uuid="0cc7bda9-bb9b-4230-997b-e5edf69560eb" x="0" y="0" width="1" height="25"/>
			</line>
			<line>
				<reportElement uuid="ce98856d-7332-4a25-be81-5b5547462578" x="424" y="0" width="1" height="25"/>
			</line>
			<line>
				<reportElement uuid="2ecc09cb-6123-4b40-8728-dc39d5e889fa" x="0" y="0" width="424" height="1"/>
			</line>
			<textField>
				<reportElement uuid="89aa685f-f96f-4e28-bb75-c6fc9504713d" x="0" y="1" width="424" height="24" forecolor="#990000"/>
				<box>
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA["Résumé global (montants en " +$F{devcompany} + ")"]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band/>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Prevent">
			<staticText>
				<reportElement uuid="9ef8a93c-be59-43ee-8f4f-45ff3f5b9e9e" mode="Opaque" x="223" y="0" width="100" height="20" backcolor="#E1E1E1"/>
				<box rightPadding="6">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Crédit]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="620f1ab2-e5d5-4e99-a988-f8f45ec31ec1" mode="Opaque" x="324" y="0" width="100" height="20" backcolor="#E1E1E1"/>
				<box rightPadding="6">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Solde]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="bb2cfb97-e431-4804-8796-77c19bf24d7c" mode="Opaque" x="122" y="0" width="100" height="20" backcolor="#E1E1E1"/>
				<box rightPadding="6">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Débit]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="9a758964-f6ef-440b-819e-853233962ffe" mode="Opaque" x="0" y="0" width="121" height="20" backcolor="#E1E1E1"/>
				<box rightPadding="6">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Type]]></text>
			</staticText>
			<line>
				<reportElement uuid="488dacfb-b45c-4dad-b229-305a28eb92cf" x="0" y="0" width="1" height="20"/>
			</line>
			<line>
				<reportElement uuid="5d392dc6-b28d-46ad-a46d-1348461fb272" x="424" y="0" width="1" height="20"/>
			</line>
			<line>
				<reportElement uuid="0c139785-a57e-4259-886e-58a5efa31571" x="121" y="0" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="d0aab714-3c35-45ed-85d9-b490593f2c49" x="222" y="0" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="8cbc9a11-9745-423c-8e4a-f521676f2ad7" x="323" y="0" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="21" splitType="Prevent">
			<frame>
				<reportElement uuid="7f9ef7b0-ef2f-4dc6-967a-6cc91413e0be" style="AlternateDetailRowStyle" x="0" y="0" width="424" height="21"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textField>
					<reportElement uuid="932062c2-6e22-4ec2-a512-7e6801f9fb05" style="encadre" x="1" y="1" width="120" height="20"/>
					<box leftPadding="3" rightPadding="6">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_type}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="3230efda-a1d0-456b-b29b-4e91431b51df" style="encadre" x="122" y="0" width="100" height="21"/>
					<box rightPadding="6">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="3adc1dd2-f0a8-4450-a1ac-96f833350179" style="encadre" x="223" y="0" width="100" height="21"/>
					<box rightPadding="6">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="845e9bd5-a97b-45bd-b438-f1d2041e532d" style="encadre" x="324" y="0" width="100" height="21"/>
					<box rightPadding="6">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{solde}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement uuid="68e39869-29c8-4c0b-a75b-feba4119bb50" x="121" y="0" width="1" height="21"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="b5160626-9a0e-43e5-bf24-110fb55fb61c" x="222" y="0" width="1" height="21"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="dc0efe7a-e91a-4e5e-848c-a9a8a6331fc6" x="323" y="0" width="1" height="21"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
			</frame>
			<line>
				<reportElement uuid="8e6f620e-9492-4593-9aed-f2cf01234f92" x="0" y="0" width="1" height="21"/>
			</line>
			<line>
				<reportElement uuid="91026935-def8-4637-95a9-46590709e619" x="424" y="0" width="1" height="21"/>
			</line>
			<line>
				<reportElement uuid="e903ef82-db4a-4737-be84-29c84534bba3" x="0" y="20" width="424" height="1"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</detail>
	<summary>
		<band height="30">
			<textField evaluationTime="Report" pattern="#,##0.00;-#,##0.00">
				<reportElement uuid="4d48fa90-801d-4cab-948c-acc137ab19db" x="122" y="1" width="100" height="28"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{debit_sum}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="#,##0.00;-#,##0.00">
				<reportElement uuid="76949846-14bf-4acd-b3c6-4ed38b87804b" x="223" y="1" width="100" height="28"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{credit_sum}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="#,##0.00;-#,##0.00">
				<reportElement uuid="ca782f7a-4594-476d-b7bc-ecf8849ad878" x="324" y="1" width="100" height="28"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{solde_sum}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="7bb1451e-ab17-46ef-a480-d69a0a1c161c" x="323" y="1" width="1" height="28"/>
			</line>
			<staticText>
				<reportElement uuid="2b84fe24-9896-4220-bf6b-fd390353329b" x="1" y="0" width="120" height="29"/>
				<box leftPadding="3" rightPadding="0"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL:]]></text>
			</staticText>
			<line>
				<reportElement uuid="6c994890-6b22-4935-842e-444170cb2b82" x="222" y="1" width="1" height="28"/>
			</line>
			<line>
				<reportElement uuid="ddaf0172-245a-4302-9722-79c55c70235b" x="0" y="29" width="425" height="1"/>
			</line>
			<line>
				<reportElement uuid="e6e1302f-19a9-4b6c-a1fd-c05963033708" x="0" y="0" width="425" height="1"/>
				<graphicElement>
					<pen lineWidth="1.0"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="7cda21d7-e9ea-40f1-9e7e-60571d534b26" x="424" y="1" width="1" height="28"/>
			</line>
			<line>
				<reportElement uuid="cc8caa40-2e0d-4fed-9795-7f47be40e00d" x="0" y="0" width="1" height="29"/>
			</line>
			<line>
				<reportElement uuid="c0bfb55e-7600-45df-9883-031b9ce82b84" x="121" y="1" width="1" height="28"/>
			</line>
		</band>
	</summary>
</jasperReport>
