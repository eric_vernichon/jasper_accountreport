<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Acount Balances" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="786" leftMargin="28" rightMargin="28" topMargin="34" bottomMargin="34" whenResourceMissingType="Error">
	<property name="ireport.jasperserver.url" value="http://localhost:18080/jasperserver/services/repository"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="18"/>
	<style name="AlternatingDetailRowStyle">
		<conditionalStyle>
			<conditionExpression><![CDATA[($V{COLUMN_COUNT}) % 2 == 1]]></conditionExpression>
			<style mode="Opaque" backcolor="#E1E1E1"/>
		</conditionalStyle>
	</style>
	<parameter name="date_start" class="java.lang.String"/>
	<parameter name="date_stop" class="java.lang.String"/>
	<parameter name="company_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[1]]></defaultValueExpression>
	</parameter>
	<parameter name="is_partner_type_client" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[true]]></defaultValueExpression>
	</parameter>
	<parameter name="include_drafts" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[false]]></defaultValueExpression>
	</parameter>
	<parameter name="account_ids" class="java.lang.String"/>
	<parameter name="partner_ids" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="filters_label" class="java.lang.String"/>
	<parameter name="currency_ids" class="java.lang.String"/>
	<parameter name="query" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[" (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS devcompany,"
+" (SELECT name FROM public.res_company WHERE id = " + $P{company_id} + ") AS companyname,"
+" (SELECT p.name FROM public.account_period p WHERE p.date_start >= '" + $P{date_start} + "' ORDER BY p.date_start LIMIT 1) AS pmin,"
+" (SELECT p.name FROM public.account_period p WHERE p.date_stop <= '" + $P{date_stop} + "'  ORDER BY p.date_stop DESC LIMIT 1) AS pmax,"
+" COALESCE(res_currency.name, (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + "))) AS res_currency_code,"
+" account_move_line.date AS account_move_line_date,"
+" account_move_line.name as account_move_line_name,"
+" account_move_line.ref as account_move_line_ref,"
+" account_move_line.debit as account_move_line_debit,"
+" account_move_line.credit as account_move_line_credit,"
+" COALESCE(account_move_line.amount_currency, 0) as account_move_line_amount_currency,"
+" account_journal.code as account_journal_code,"
+" account_move_reconcile.name as account_move_reconcile_name,"
+" account_move_reconcile_partial.name AS account_move_reconcile_partial_name,"
+" res_partner.name as res_partner_name"
+" FROM public.account_move account_move"
+" INNER JOIN public.account_move_line account_move_line ON account_move.id = account_move_line.move_id"
+" INNER JOIN public.account_account account_account ON account_move_line.account_id = account_account.id"
+" INNER JOIN public.account_period account_period ON account_period.id = account_move_line.period_id"
+" LEFT OUTER JOIN res_currency res_currency ON account_move_line.currency_id = res_currency.id"
+" LEFT OUTER JOIN account_move_reconcile account_move_reconcile ON account_move_line.reconcile_id = account_move_reconcile.id"
+" LEFT OUTER JOIN account_move_reconcile account_move_reconcile_partial ON account_move_line.reconcile_partial_id = account_move_reconcile_partial.id"
+" LEFT OUTER JOIN res_partner res_partner ON account_move_line.partner_id = res_partner.id"
+" INNER JOIN account_journal account_journal ON account_move.journal_id = account_journal.id"
+" WHERE account_move_line.account_id IN " + $P{account_ids}
+" AND " + ($P{is_partner_type_client} ? "res_partner.customer = True" : "res_partner.supplier = True")
+($P{include_drafts}? "" : " AND account_move_line.state != 'draft'")
+" AND account_period.date_start >= '" + $P{date_start} + "'"
+" AND account_period.date_stop <= '" + $P{date_stop} +"'"
+(!$P{partner_ids}.isEmpty() ? " AND res_partner.id IN " + $P{partner_ids} : "")
+(!$P{currency_ids}.isEmpty() ? " AND res_currency.id IN " + $P{currency_ids} : "")
+" UNION ALL"
+" SELECT (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")),"
+" (SELECT name FROM public.res_company WHERE id = " + $P{company_id} + "),"
+" (SELECT p.name FROM public.account_period p WHERE p.date_start >= '" + $P{date_start} + "' ORDER BY p.date_start LIMIT 1),"
+" (SELECT p.name FROM public.account_period p WHERE p.date_stop <= '" + $P{date_stop} +"'  ORDER BY p.date_stop DESC LIMIT 1),"
+" (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS res_currency_code,"
+" '" + $P{date_start} + "',"
+" '   ---   Solde de départ   ---   ',"
+" '',"
+" COALESCE((SELECT SUM(account_move_line.debit)"
+"		FROM public.account_move_line account_move_line"
+"		INNER JOIN public.account_move account_move ON account_move.id = account_move_line.move_id"
+"		INNER JOIN public.account_account account_account ON account_move_line.account_id = account_account.id"
+"		INNER JOIN public.account_period account_period ON account_period.id = account_move_line.period_id"
+"		WHERE account_move_line.account_id IN " + $P{account_ids}
+"		AND account_move_line.partner_id = A.id"
+"		AND account_period.date_stop < '" + $P{date_start} + "'), 0),"
+" COALESCE((SELECT SUM(account_move_line.credit)"
+"		FROM public.account_move_line account_move_line"
+"		INNER JOIN public.account_move account_move ON account_move.id = account_move_line.move_id"
+"		INNER JOIN public.account_account account_account ON account_move_line.account_id = account_account.id"
+"		INNER JOIN public.account_period account_period ON account_period.id = account_move_line.period_id"
+"		WHERE account_move_line.account_id IN " + $P{account_ids}
+"		AND account_move_line.partner_id = A.id"
+"		AND account_period.date_stop < '" + $P{date_start} + "'), 0),"
+" 0,"
+" '',"
+" '',"
+" '',"
+" A.name"
+" FROM public.res_partner A"
+" WHERE "
+ (!$P{partner_ids}.isEmpty() ? "A.id IN " + $P{partner_ids} : " EXISTS (SELECT account_move.id "
+" FROM public.account_move account_move"
+" INNER JOIN public.account_move_line account_move_line ON account_move.id = account_move_line.move_id"
+" INNER JOIN public.account_account account_account ON account_move_line.account_id = account_account.id"
+" INNER JOIN public.account_period account_period ON account_period.id = account_move_line.period_id"
+" LEFT OUTER JOIN res_currency res_currency ON account_move_line.currency_id = res_currency.id"
+" LEFT OUTER JOIN account_move_reconcile account_move_reconcile ON account_move_line.reconcile_id = account_move_reconcile.id"
+" LEFT OUTER JOIN account_move_reconcile account_move_reconcile_partial ON account_move_line.reconcile_partial_id = account_move_reconcile_partial.id"
+" LEFT OUTER JOIN res_partner res_partner ON account_move_line.partner_id = res_partner.id"
+" INNER JOIN account_journal account_journal ON account_move.journal_id = account_journal.id"
+" WHERE account_move_line.account_id IN " + $P{account_ids}
+" AND res_partner.id = A.id"
+" AND account_period.date_start >= '" + $P{date_start} + "'"
+" AND account_period.date_stop <= '" + $P{date_stop} + "')"
+(!$P{currency_ids}.isEmpty() ? " AND res_currency.id IN " + $P{currency_ids} : ""))
+" ORDER BY res_partner_name, account_move_line_date"]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:\\Users\\svalaeys.FIEF\\AppData\\Local\\Temp\\jstmp\\"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT $P!{query}]]>
	</queryString>
	<field name="companyname" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="pmin" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="pmax" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="res_currency_code" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="account_move_line_debit" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="account_move_line_credit" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="account_move_line_date" class="java.util.Date"/>
	<field name="account_move_line_name" class="java.lang.String"/>
	<field name="account_move_line_ref" class="java.lang.String"/>
	<field name="account_move_line_amount_currency" class="java.math.BigDecimal"/>
	<field name="account_journal_code" class="java.lang.String"/>
	<field name="account_move_reconcile_name" class="java.lang.String"/>
	<field name="account_move_reconcile_partial_name" class="java.lang.String"/>
	<field name="res_partner_name" class="java.lang.String"/>
	<field name="devcompany" class="java.lang.String"/>
	<variable name="cumulative_balance" class="java.math.BigDecimal" resetType="Group" resetGroup="Partner subtotal" calculation="Sum">
		<variableExpression><![CDATA[$F{account_move_line_debit}.subtract( $F{account_move_line_credit} )]]></variableExpression>
	</variable>
	<variable name="partner_sum_credit" class="java.math.BigDecimal" resetType="Group" resetGroup="Partner subtotal" calculation="Sum">
		<variableExpression><![CDATA[$F{account_move_line_credit}]]></variableExpression>
	</variable>
	<variable name="partner_sum_debit" class="java.math.BigDecimal" resetType="Group" resetGroup="Partner subtotal" calculation="Sum">
		<variableExpression><![CDATA[$F{account_move_line_debit}]]></variableExpression>
	</variable>
	<variable name="total_sum_credit" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{account_move_line_credit}]]></variableExpression>
	</variable>
	<variable name="total_sum_debit" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{account_move_line_debit}]]></variableExpression>
	</variable>
	<group name="Partner subtotal" keepTogether="true">
		<groupExpression><![CDATA[$F{res_partner_name}]]></groupExpression>
		<groupHeader>
			<band height="17" splitType="Prevent">
				<frame>
					<reportElement mode="Transparent" x="0" y="0" width="786" height="15" backcolor="#DD7F7F"/>
					<textField>
						<reportElement mode="Transparent" x="0" y="0" width="346" height="15" forecolor="#990000" backcolor="#FFFFFF"/>
						<textElement verticalAlignment="Middle">
							<font fontName="Gisha" size="10" isPdfEmbedded="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{res_partner_name}.toUpperCase()]]></textFieldExpression>
					</textField>
				</frame>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="25" splitType="Prevent">
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="538" y="0" width="1" height="20">
						<printWhenExpression><![CDATA[false]]></printWhenExpression>
					</reportElement>
				</line>
				<line>
					<reportElement x="619" y="0" width="1" height="20"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="700" y="0" width="1" height="20"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<textField>
					<reportElement x="83" y="0" width="449" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Solde en fin de période pour " + $F{res_partner_name} + ":"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="0" width="786" height="1"/>
				</line>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="539" y="0" width="78" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{partner_sum_debit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="620" y="0" width="78" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{partner_sum_credit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="701" y="0" width="83" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{cumulative_balance}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="85">
			<staticText>
				<reportElement x="0" y="36" width="45" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Société :]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="47" y="36" width="340" height="20" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="56" width="277" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Période de "+$F{pmin} + " à " + $F{pmax}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="32" width="786" height="1"/>
			</line>
			<textField>
				<reportElement x="0" y="0" width="424" height="32" forecolor="#990000"/>
				<textElement>
					<font fontName="Gisha" size="20" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Grand livre des " + ($P{is_partner_type_client} ? "clients" : "fournisseurs")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="397" y="39" width="389" height="40" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Top">
					<font fontName="Gisha" size="7" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{filters_label}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="20">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} !=1]]></printWhenExpression>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="15" width="786" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement x="0" y="0" width="424" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Grand livre des tiers de " + $F{pmin} + " à " + $F{pmax}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="437" y="0" width="348" height="13" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="35">
			<staticText>
				<reportElement mode="Opaque" x="0" y="15" width="50" height="20" backcolor="#E1E1E1"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="51" y="15" width="45" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Journal]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="323" y="15" width="49" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Rec]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="423" y="15" width="39" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Devise]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="539" y="15" width="80" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Debit]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="620" y="15" width="80" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Crédit]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="701" y="15" width="85" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Solde]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="463" y="15" width="75" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Montant Devise]]></text>
			</staticText>
			<line>
				<reportElement x="50" y="15" width="1" height="20"/>
			</line>
			<line>
				<reportElement x="538" y="0" width="1" height="35"/>
			</line>
			<staticText>
				<reportElement mode="Transparent" x="423" y="0" width="115" height="15" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.0" lineStyle="Solid"/>
					<rightPen lineWidth="0.0" lineStyle="Solid"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Devise des opérations]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="163" y="15" width="159" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Libellé]]></text>
			</staticText>
			<line>
				<reportElement x="162" y="15" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="619" y="15" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="700" y="15" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement x="539" y="0" width="246" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Devise de base: " + $F{devcompany}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="97" y="15" width="65" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Référence]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="373" y="15" width="49" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Rec P]]></text>
			</staticText>
			<line>
				<reportElement x="322" y="15" width="1" height="20"/>
			</line>
			<line>
				<reportElement x="372" y="15" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="96" y="15" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="462" y="15" width="1" height="20"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="423" y="0" width="1" height="35"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Prevent">
			<frame>
				<reportElement key="FrameDetails" style="AlternatingDetailRowStyle" stretchType="RelativeToTallestObject" x="0" y="0" width="786" height="15"/>
				<textField pattern="dd/MM/yyyy">
					<reportElement x="0" y="0" width="48" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_line_date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement x="52" y="0" width="43" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_journal_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="423" y="0" width="37" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code}) != 0]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{res_currency_code}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false">
					<reportElement x="701" y="0" width="83" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{cumulative_balance}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement x="373" y="0" width="47" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_reconcile_partial_name}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="539" y="0" width="78" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_line_debit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="620" y="0" width="78" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_line_credit}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="97" y="0" width="63" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_line_ref}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="463" y="0" width="73" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code}) != 0]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_line_amount_currency}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement x="323" y="0" width="47" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_reconcile_name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement x="163" y="0" width="157" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_move_line_name}]]></textFieldExpression>
				</textField>
			</frame>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="619" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="700" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="462" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="50" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="322" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="422" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="372" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="538" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="96" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="162" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</detail>
	<pageFooter>
		<band height="20">
			<textField>
				<reportElement x="617" y="4" width="128" height="13"/>
				<textElement textAlignment="Right">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="746" y="4" width="40" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[" de " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="0" y="4" width="190" height="13"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Date d'édition: " + (new SimpleDateFormat("E dd MMMM yyyy")).format(new java.util.Date())]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="2" width="786" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineStyle="Double" lineColor="#CCCCFF"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band height="198" splitType="Stretch">
			<line>
				<reportElement x="0" y="3" width="785" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="408" y="10" width="131" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Total général:]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="6" width="785" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="622" y="10" width="1" height="28"/>
			</line>
			<line>
				<reportElement x="702" y="10" width="1" height="28"/>
			</line>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="539" y="10" width="78" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_sum_debit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="623" y="10" width="78" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_sum_credit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="703" y="10" width="83" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_sum_debit}.subtract( $V{total_sum_credit} )]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement x="124" y="80" width="539" height="118" isRemoveLineWhenBlank="true"/>
				<subreportParameter name="currency_ids">
					<subreportParameterExpression><![CDATA[$P{currency_ids}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="is_partner_type_client">
					<subreportParameterExpression><![CDATA[$P{is_partner_type_client}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="company_id">
					<subreportParameterExpression><![CDATA[$P{company_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="date_stop">
					<subreportParameterExpression><![CDATA[$P{date_stop}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="account_ids">
					<subreportParameterExpression><![CDATA[$P{account_ids}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="date_start">
					<subreportParameterExpression><![CDATA[$P{date_start}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="include_drafts">
					<subreportParameterExpression><![CDATA[$P{include_drafts}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="partner_ids">
					<subreportParameterExpression><![CDATA[$P{partner_ids}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA["repo:/Reports/partners_ledger_w_currency_details_subreport"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
