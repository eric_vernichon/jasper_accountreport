<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Partners Balance With Currency Details" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="786" leftMargin="28" rightMargin="28" topMargin="34" bottomMargin="34">
	<property name="ireport.jasperserver.url" value="http://192.168.12.17:18080/jasperserver/services/repository"/>
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="74"/>
	<property name="ireport.y" value="62"/>
	<style name="AlternatingDetailRowStyle">
		<conditionalStyle>
			<conditionExpression><![CDATA[($V{REPORT_COUNT}) % 2 == 1]]></conditionExpression>
			<style mode="Opaque" backcolor="#E1E1E1"/>
		</conditionalStyle>
	</style>
	<parameter name="period_start_id" class="java.lang.Integer"/>
	<parameter name="period_stop_id" class="java.lang.Integer"/>
	<parameter name="is_partner_type_client" class="java.lang.Boolean"/>
	<parameter name="company_id" class="java.lang.Integer"/>
	<parameter name="account_ids" class="java.lang.String"/>
	<parameter name="period_ids" class="java.lang.String"/>
	<parameter name="currency_ids" class="java.lang.String"/>
	<parameter name="partner_ids" class="java.lang.String"/>
	<parameter name="account_labels" class="java.lang.String"/>
	<parameter name="subtotal" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="print_zeroes" class="java.lang.Boolean"/>
	<parameter name="query" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["SELECT DISTINCT (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS devcompany,"
+" (SELECT name FROM public.res_company WHERE id = " + $P{company_id} + ") AS companyname,"
+" (SELECT p.name FROM public.account_period p WHERE p.id = " + $P{period_start_id} + ") AS pmin,"
+" (SELECT p.name FROM public.account_period p WHERE p.id = " + $P{period_stop_id} + ") AS pmax,"
+" res_currency.name AS res_currency_code,"
+" account.code AS account_account_code,"
+" account.name AS account_account_name,"
+" res_partner.name AS res_partner_name,"
+" COALESCE(previous.base_balance, 0) AS base_previous_balance,"
+" COALESCE(previous.ccy_balance, 0) AS ccy_previous_balance,"
+" COALESCE(lines.debit, 0) AS debit,"
+" COALESCE(lines.credit, 0) AS credit,"
+" COALESCE(lines.solde, 0) AS solde,"
+" COALESCE(lines.amount_currency, 0) AS amount_currency,"
+" CASE WHEN res_partner.id IS NULL THEN 0 ELSE COALESCE(previous.ccy_balance, 0) + COALESCE(lines.amount_currency, 0) END AS ccy_effective_balance,"
+" CASE WHEN res_partner.id IS NULL THEN 0 ELSE COALESCE(previous.base_balance, 0) + COALESCE(lines.solde, 0) END AS base_effective_balance"
+" FROM public.account_account account"
+" INNER JOIN public.account_move_line line ON account.id = line.account_id"
+" LEFT JOIN public.res_partner res_partner ON line.partner_id = res_partner.id"
+" LEFT OUTER JOIN public.res_currency res_currency ON COALESCE(line.currency_id, (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) = res_currency.id"
+" LEFT OUTER JOIN"
+" (SELECT line.account_id,"
+" line.partner_id,"
+" COALESCE(line.currency_id, (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS currency_id,"
+" SUM(line.debit) AS debit,"
+" SUM(line.credit) AS credit,"
+" SUM(line.debit) - SUM(line.credit) AS solde,"
+" SUM(COALESCE(line.amount_currency, 0)) AS amount_currency"
+" FROM public.account_move_line line"
+" INNER JOIN public.account_account account_account ON account_account.id = line.account_id"
+" WHERE line.state != 'draft'"
+" AND line.period_id IN " + $P{period_ids}
+" AND line.account_id IN " + $P{account_ids}
+" GROUP BY line.account_id, line.partner_id, COALESCE(line.currency_id, (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + "))"
+" )lines ON lines.account_id = account.id AND lines.partner_id IS NOT DISTINCT FROM res_partner.id AND lines.currency_id = res_currency.id"
+" LEFT OUTER JOIN"
+" (SELECT aml.account_id, "
+"         aml.partner_id, "
+"         COALESCE(aml.currency_id, (SELECT currency_id FROM public.res_company WHERE id =" + $P{company_id} + ")) AS currency_id, "
+"         SUM(COALESCE(aml.amount_currency, 0)) AS ccy_balance,"
+"         SUM(aml.debit - aml.credit) AS base_balance"
+"		   FROM public.account_move_line aml"
+"         INNER JOIN public.account_period period ON aml.period_id = period.id"
+"		   WHERE period.date_stop < (SELECT p.date_start FROM public.account_period p WHERE p.id = " + $P{period_start_id} +")"
+"		   AND aml.state='valid'"
+"         AND aml.account_id IN " + $P{account_ids}
+"		   GROUP BY aml.account_id, aml.partner_id, COALESCE(aml.currency_id, (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + "))) previous ON previous.account_id = account.id AND previous.partner_id = res_partner.id AND previous.currency_id = res_currency.id"
+" WHERE account.id IN " + $P{account_ids}
+($P{partner_ids}.isEmpty() ?"" : (" AND res_partner.id IN " + $P{partner_ids}))
+($P{currency_ids}.isEmpty() ?"" : (" AND res_currency.id IN " + $P{currency_ids}))
+($P{print_zeroes}? "" : " AND (COALESCE(previous.base_balance, 0) !=0 OR COALESCE(lines.debit, 0) !=0 OR COALESCE(lines.credit, 0) != 0)")
+" ORDER BY account.code, res_partner.name, res_currency_code"]]></defaultValueExpression>
	</parameter>
	<parameter name="partner_labels" class="java.lang.String"/>
	<parameter name="currency_labels" class="java.lang.String"/>
	<queryString>
		<![CDATA[$P!{query}]]>
	</queryString>
	<field name="devcompany" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="companyname" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="pmin" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="pmax" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="account_account_code" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="debit" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="credit" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="solde" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="res_partner_name" class="java.lang.String"/>
	<field name="amount_currency" class="java.math.BigDecimal"/>
	<field name="res_currency_code" class="java.lang.String"/>
	<field name="base_effective_balance" class="java.math.BigDecimal"/>
	<field name="ccy_effective_balance" class="java.math.BigDecimal"/>
	<field name="base_previous_balance" class="java.math.BigDecimal"/>
	<field name="ccy_previous_balance" class="java.math.BigDecimal"/>
	<sortField name="account_account_code"/>
	<variable name="sum_balance" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{solde}]]></variableExpression>
	</variable>
	<variable name="sum_debit" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<variable name="sum_credit" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<variable name="sum_balance_account" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{solde}]]></variableExpression>
	</variable>
	<variable name="sum_credit_account" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<variable name="sum_debit_account" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<variable name="sum_prev_balance_account" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{base_previous_balance}]]></variableExpression>
	</variable>
	<variable name="sum_effective_balance_account" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{base_effective_balance}]]></variableExpression>
	</variable>
	<variable name="sum_prev_balance" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{base_previous_balance}]]></variableExpression>
	</variable>
	<variable name="sum_effective_balance" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{base_effective_balance}]]></variableExpression>
	</variable>
	<group name="Classe sous total">
		<groupExpression><![CDATA[$F{account_account_code}]]></groupExpression>
		<groupFooter>
			<band height="23">
				<printWhenExpression><![CDATA[$P{subtotal}]]></printWhenExpression>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="643" y="1" width="1" height="22"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="644" y="0" width="71" height="22"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_balance_account}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="576" y="1" width="67" height="22"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_credit_account}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="508" y="1" width="67" height="22"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_debit_account}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="47" y="1" width="383" height="22"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total compte " +$F{account_account_code} + ": "]]></textFieldExpression>
				</textField>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="441" y="0" width="1" height="23"/>
				</line>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="575" y="0" width="1" height="23"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="0" y="0" width="786" height="1"/>
				</line>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="45" y="1" width="1" height="22"/>
				</line>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="716" y="1" width="70" height="22"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_effective_balance_account}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="715" y="1" width="1" height="22"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="443" y="1" width="64" height="22"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_prev_balance_account}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="507" y="0" width="1" height="23"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="85">
			<staticText>
				<reportElement x="0" y="0" width="370" height="25" forecolor="#990000"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Gisha" size="20" isBold="false" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Balance des soldes des comptes de tiers]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="36" width="45" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Société :]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="47" y="36" width="210" height="20" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="56" width="242" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Période de "+$F{pmin}+ " à " + $F{pmax}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="32" width="786" height="1"/>
			</line>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="369" y="39" width="275" height="40" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Top">
					<font fontName="Gisha" size="7" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Comptes " + $P{account_labels}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="20">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} !=1]]></printWhenExpression>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="15" width="786" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement x="0" y="0" width="316" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Balance des soldes des comptes de tiers de " + $F{pmin} + " à " + $F{pmax}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="579" y="0" width="207" height="13" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="48">
			<staticText>
				<reportElement mode="Opaque" x="0" y="15" width="45" height="25" backcolor="#E1E1E1"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[N° compte]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="46" y="15" width="156" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{is_partner_type_client} ? "Client" : "Fournisseur")]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="442" y="0" width="344" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Devise de base: " + $F{devcompany}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="203" y="0" width="238" height="15" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.0" lineStyle="Solid"/>
					<rightPen lineWidth="0.0" lineStyle="Solid"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Devise des opérations]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="203" y="15" width="30" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Devise]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="716" y="15" width="70" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Solde effectif"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="644" y="15" width="71" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Solde comptable sur la période"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="576" y="15" width="67" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Crédit sur la période"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="508" y="15" width="67" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Débit sur la période"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="442" y="15" width="65" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Solde préalable"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="575" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="643" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="202" y="0" width="1" height="48"/>
			</line>
			<line>
				<reportElement x="45" y="15" width="1" height="33"/>
			</line>
			<line>
				<reportElement x="441" y="0" width="1" height="48"/>
			</line>
			<line>
				<reportElement x="233" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="715" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="507" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement mode="Opaque" x="371" y="15" width="70" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Solde effectif"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="300" y="15" width="70" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Solde comptable sur la période"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="370" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement mode="Opaque" x="234" y="15" width="65" height="25" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Solde préalable"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="299" y="15" width="1" height="33"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Prevent">
			<frame>
				<reportElement key="FrameDetails" style="AlternatingDetailRowStyle" stretchType="RelativeToTallestObject" x="0" y="0" width="786" height="15"/>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="644" y="0" width="71" height="15"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{solde}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="45" height="15"/>
					<box leftPadding="3"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_account_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement stretchType="RelativeToTallestObject" x="46" y="0" width="156" height="15"/>
					<box leftPadding="3"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{res_partner_name}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="508" y="0" width="67" height="15"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="576" y="0" width="67" height="15"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="300" y="0" width="70" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code} )!=0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{amount_currency}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="299" y="0" width="1" height="15"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<textField>
					<reportElement stretchType="RelativeToTallestObject" x="203" y="0" width="30" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo($F{res_currency_code} )!=0]]></printWhenExpression>
					</reportElement>
					<box leftPadding="1"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{res_currency_code}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="716" y="0" width="70" height="15"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{base_effective_balance}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="442" y="0" width="65" height="15"/>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{base_previous_balance}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="370" y="0" width="1" height="15"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="371" y="0" width="70" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code} )!=0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ccy_effective_balance}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="234" y="0" width="65" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code} )!=0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ccy_previous_balance}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement stretchType="RelativeToBandHeight" x="233" y="0" width="1" height="15"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
			</frame>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="45" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="441" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="507" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="643" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="202" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="715" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="575" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</detail>
	<pageFooter>
		<band height="16">
			<textField>
				<reportElement x="625" y="3" width="128" height="13"/>
				<textElement textAlignment="Right">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="754" y="3" width="31" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[" de " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="0" y="3" width="190" height="13"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Date d'édition: " + (new SimpleDateFormat("E dd MMMM yyyy")).format(new java.util.Date())]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="1" width="786" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineWidth="0.5" lineStyle="Solid" lineColor="#999999"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band height="38" splitType="Stretch">
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="644" y="10" width="71" height="28"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sum_balance}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="576" y="10" width="67" height="28"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sum_credit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="508" y="10" width="67" height="28"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sum_debit}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="338" y="10" width="98" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Totaux:]]></text>
			</staticText>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="716" y="10" width="70" height="28"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sum_effective_balance}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="3" width="786" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="0" y="6" width="786" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="575" y="10" width="1" height="28"/>
			</line>
			<line>
				<reportElement x="643" y="10" width="1" height="28"/>
			</line>
			<line>
				<reportElement x="715" y="10" width="1" height="28"/>
			</line>
			<line>
				<reportElement x="507" y="10" width="1" height="28"/>
			</line>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="442" y="10" width="65" height="28"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sum_prev_balance}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="441" y="10" width="1" height="28"/>
			</line>
		</band>
	</summary>
</jasperReport>
