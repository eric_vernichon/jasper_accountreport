<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="General Ledger" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="539" leftMargin="28" rightMargin="28" topMargin="34" bottomMargin="34" whenResourceMissingType="Error">
	<property name="ireport.jasperserver.url" value="http://aws-eu-1.uniforme.ch:18080/jasperserver/services/repository"/>
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="AlternatingDetailRowStyle">
		<conditionalStyle>
			<conditionExpression><![CDATA[($V{COLUMN_COUNT}) % 2 == 1]]></conditionExpression>
			<style mode="Opaque" backcolor="#E1E1E1"/>
		</conditionalStyle>
	</style>
	<parameter name="date_start" class="java.lang.String">
		<defaultValueExpression><![CDATA["2012-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="date_stop" class="java.lang.String">
		<defaultValueExpression><![CDATA["2012-09-30"]]></defaultValueExpression>
	</parameter>
	<parameter name="company_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[1]]></defaultValueExpression>
	</parameter>
	<parameter name="account_ids" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="filters_label" class="java.lang.String"/>
	<parameter name="query" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["SELECT (SELECT code FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS devcompany,"
+" (SELECT name FROM public.res_company WHERE id = " + $P{company_id} + ") AS companyname,"
+" account.name AS account_account_name,"
+" account.code AS account_account_code,"
+ "general.code || ' - ' || general.name AS account_general,"
+ "CASE acc_type.code WHEN 'income' THEN 'Revenus' ELSE 'Charges' END AS acc_type,"
+" SUM(aal.amount) AS aal_amount"
+" FROM account_analytic_line aal"
+" INNER JOIN public.account_analytic_account account ON aal.account_id = account.id"
+" INNER JOIN public.account_account general ON aal.general_account_id = general.id"
+" INNER JOIN public.account_account_type acc_type ON general.user_type = acc_type.id"
+" WHERE acc_type.code IN ('income', 'expense') "
+" AND aal.date BETWEEN '" + $P{date_start} + "' AND '" + $P{date_stop} +"'"
+(!$P{account_ids}.isEmpty() ? (" AND aal.account_id IN " + $P{account_ids}): "")
+" GROUP BY account.name, account.code, acc_type.code, general.code, general.name"
+" ORDER BY account_account_code, acc_type.code, general.code"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[$P!{query}]]>
	</queryString>
	<field name="devcompany" class="java.lang.String"/>
	<field name="companyname" class="java.lang.String"/>
	<field name="account_account_name" class="java.lang.String"/>
	<field name="account_account_code" class="java.lang.String"/>
	<field name="account_general" class="java.lang.String"/>
	<field name="aal_amount" class="java.math.BigDecimal"/>
	<field name="acc_type" class="java.lang.String"/>
	<variable name="account_sum" class="java.math.BigDecimal" resetType="Group" resetGroup="Account subtotal" calculation="Sum">
		<variableExpression><![CDATA[$F{aal_amount}]]></variableExpression>
	</variable>
	<variable name="total_sum" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{aal_amount}]]></variableExpression>
	</variable>
	<variable name="cumulative_balance" class="java.math.BigDecimal" resetType="Group" resetGroup="Account subtotal" calculation="Sum">
		<variableExpression><![CDATA[$F{aal_amount}]]></variableExpression>
	</variable>
	<variable name="group_page_count" class="java.lang.Integer" resetType="Group" resetGroup="Account subtotal" incrementType="Group" incrementGroup="Account subtotal">
		<variableExpression><![CDATA[$V{PAGE_COUNT}]]></variableExpression>
	</variable>
	<variable name="account_type_sum" class="java.math.BigDecimal" resetType="Group" resetGroup="account_type" calculation="Sum">
		<variableExpression><![CDATA[$F{aal_amount}]]></variableExpression>
	</variable>
	<group name="Account subtotal" isReprintHeaderOnEachPage="true" keepTogether="true">
		<groupExpression><![CDATA[$F{account_account_code}]]></groupExpression>
		<groupHeader>
			<band height="23" splitType="Prevent">
				<frame>
					<reportElement mode="Transparent" x="0" y="0" width="539" height="23" backcolor="#DD7F7F"/>
					<textField>
						<reportElement mode="Transparent" x="0" y="4" width="346" height="15" forecolor="#990000" backcolor="#FFFFFF"/>
						<textElement verticalAlignment="Middle">
							<font fontName="Gisha" size="10" isPdfEmbedded="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{account_account_code} + "   " + $F{account_account_name} + ($V{group_page_count}== null ? "" : " (suite)")]]></textFieldExpression>
					</textField>
				</frame>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="27" splitType="Prevent">
				<textField>
					<reportElement x="1" y="3" width="335" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Solde sur la période pour le compte " + $F{account_account_code} + ": "]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="1" width="539" height="1"/>
				</line>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="337" y="3" width="101" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{account_sum}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="account_type">
		<groupExpression><![CDATA[$F{acc_type}]]></groupExpression>
		<groupFooter>
			<band height="17">
				<textField>
					<reportElement x="0" y="1" width="338" height="14"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total " + $F{acc_type} + ": "]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement x="337" y="0" width="101" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{account_type_sum}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="0" width="539" height="1"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="85">
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="0" y="36" width="306" height="20" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Société: " +$F{companyname}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="56" width="277" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Écritures datées du "+ $P{date_start} + " au " + $P{date_stop}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="32" width="539" height="1"/>
			</line>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="315" y="39" width="224" height="40" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Top">
					<font fontName="Gisha" size="7" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{filters_label}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="2" width="358" height="30" forecolor="#990000"/>
				<textElement>
					<font fontName="Gisha" size="20"/>
				</textElement>
				<text><![CDATA[Grand livre des comptes analytiques]]></text>
			</staticText>
			<textField>
				<reportElement x="374" y="0" width="165" height="23" forecolor="#666666"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Gisha" size="12" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Montants en " + $F{devcompany}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="20">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} !=1]]></printWhenExpression>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="15" width="539" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement x="0" y="0" width="190" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Grand livre des comptes du " + $P{date_start} + " au " + $P{date_stop}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="306" y="0" width="233" height="13" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20">
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="337" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Code et libellé du compte de comptabilité générale]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="338" y="0" width="100" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Solde]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="439" y="0" width="100" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Solde cumulé]]></text>
			</staticText>
			<line>
				<reportElement x="337" y="0" width="1" height="20"/>
			</line>
			<line>
				<reportElement x="438" y="0" width="1" height="20"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Prevent">
			<frame>
				<reportElement key="FrameDetails" style="AlternatingDetailRowStyle" stretchType="RelativeToTallestObject" x="0" y="0" width="539" height="15"/>
				<textField pattern="dd/MM/yyyy">
					<reportElement stretchType="RelativeToTallestObject" x="1" y="0" width="336" height="15"/>
					<box leftPadding="1"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_general}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false">
					<reportElement stretchType="RelativeToTallestObject" x="439" y="0" width="100" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{cumulative_balance}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement stretchType="RelativeToTallestObject" x="338" y="0" width="100" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false" isPdfEmbedded="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{aal_amount}]]></textFieldExpression>
				</textField>
			</frame>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="438" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="1.0"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="337" y="0" width="1" height="15"/>
			</line>
		</band>
	</detail>
	<pageFooter>
		<band height="20">
			<textField>
				<reportElement x="370" y="4" width="128" height="13"/>
				<textElement textAlignment="Right">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="499" y="4" width="40" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[" de " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="0" y="4" width="190" height="13"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="8" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Date d'édition: " + (new SimpleDateFormat("E dd MMMM yyyy")).format(new java.util.Date())]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="1" width="539" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineWidth="0.5" lineStyle="Solid" lineColor="#999999"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band height="42" splitType="Stretch">
			<line>
				<reportElement x="0" y="3" width="539" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="190" y="10" width="146" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="10" isBold="true" isPdfEmbedded="true"/>
				</textElement>
				<text><![CDATA[Total général sur la période: ]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="6" width="539" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="338" y="10" width="100" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="10" isPdfEmbedded="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_sum}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
