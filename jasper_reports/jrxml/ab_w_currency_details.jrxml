<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Acount Balances With Currency" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="786" leftMargin="28" rightMargin="28" topMargin="34" bottomMargin="34" uuid="16afcfa0-9a90-482c-ace9-efcece1bc688">
	<property name="ireport.jasperserver.url" value="http://aws-eu-1.uniforme.ch:18080/jasperserver/services/repository"/>
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="AlternatingDetailRowStyle">
		<conditionalStyle>
			<conditionExpression><![CDATA[($V{COLUMN_COUNT}) % 2 == 1]]></conditionExpression>
			<style mode="Opaque" backcolor="#E1E1E1"/>
		</conditionalStyle>
	</style>
	<parameter name="period_start_id" class="java.lang.Integer"/>
	<parameter name="period_stop_id" class="java.lang.Integer"/>
	<parameter name="period_ids" class="java.lang.String"/>
	<parameter name="company_id" class="java.lang.Integer"/>
	<parameter name="print_zeroes" class="java.lang.Boolean"/>
	<parameter name="subtotal" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="limit_to_accounts" class="java.lang.String"/>
	<parameter name="general_account_ids" class="java.lang.String"/>
	<parameter name="analytic_account_ids" class="java.lang.String"/>
	<parameter name="query" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["WITH RECURSIVE accounts AS"
+" (SELECT 0 AS level, ac.code AS class, ac.id, ac.parent_id, ac.code, ac.active, ac.type, ac.name, ac.user_type, ac.currency_id"
+" FROM public.account_account ac WHERE ac.parent_id IS NULL AND ac.active = true"
+" UNION ALL"
+" SELECT accounts.level + 1 AS level, CASE WHEN accounts.level + 1 = 2 THEN ac.code ELSE accounts.class END AS class, ac.id, ac.parent_id, ac.code, ac.active, ac.type, ac.name, ac.user_type, ac.currency_id"
+" FROM public.account_account ac, accounts WHERE ac.parent_id = accounts.id AND ac.active = true)"
+"SELECT (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")) AS devcompany,"
+" (SELECT name FROM public.res_company WHERE id = " + $P{company_id} + ") AS companyname,"
+" (SELECT p.name FROM public.account_period p WHERE p.id = " + $P{period_start_id} + ") AS pmin,"
+" (SELECT p.name FROM public.account_period p WHERE p.id = " + $P{period_stop_id} + ") AS pmax,"
+" accounts.code AS account_account_code,"
+" accounts.name AS account_account_name,"
+" accounts.class AS account_account_class,"
+" (SELECT name FROM public.res_currency WHERE id = accounts.currency_id) AS account_ccy,"
+" COALESCE(lines.currency_code, (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + "))) AS res_currency_code,"
+" COALESCE(lines.debit, 0) AS debit,"
+" COALESCE(lines.credit, 0) AS credit,"
+" COALESCE(lines.debit - lines.credit, 0) AS solde,"
+" COALESCE(lines.debitdev, 0) AS debitdev,"
+" COALESCE(lines.creditdev, 0) AS creditdev,"
+" COALESCE(lines.soldedevise, 0) AS soldedevise"
+" FROM accounts "
+" INNER JOIN public.account_account_type account_type ON account_type.id = accounts.user_type"
+ ($P{print_zeroes} ? " LEFT JOIN" : " INNER JOIN")
+" (SELECT line.account_id,"
+"  COALESCE(res_currency.name , (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + "))) AS currency_code,"
+"  SUM(line.debit) AS debit,"
+"  SUM(line.credit) AS credit,"
+"  SUM(line.debit-line.credit) AS solde,"
+"  SUM(CASE line.amount_currency != ABS(line.amount_currency)"
+"     WHEN True THEN 0  "
+"     ELSE COALESCE(line.amount_currency, 0)"
+"     END) as debitdev,"
+"  SUM(CASE line.amount_currency  = ABS(line.amount_currency) "
+"     WHEN True THEN 0  "
+"     ELSE ABS(COALESCE(line.amount_currency, 0))"
+"     END) as creditdev,"
+"  SUM(COALESCE(line.amount_currency, 0)) as soldedevise "
+"  FROM public.account_move_line line "
+"  INNER JOIN public.account_period account_period ON account_period.id = line.period_id"
+"  LEFT JOIN public.res_currency res_currency ON res_currency.id = line.currency_id"
+"  WHERE line.state != 'draft'"
+"  AND account_period.id IN " + $P{period_ids}
+ ($P{analytic_account_ids} == null || $P{analytic_account_ids}.isEmpty() ? "": (" AND line.analytic_account_id IN " + $P{analytic_account_ids}))
+" GROUP BY line.account_id, COALESCE(res_currency.name, (SELECT name FROM public.res_currency WHERE id = (SELECT currency_id FROM public.res_company WHERE id = " + $P{company_id} + ")))"
+" ) lines ON accounts.id = lines.account_id"
+" WHERE accounts.type != 'view'   "
+" AND account_type.code != 'special'"
+" AND accounts.active = true"
+ ($P{limit_to_accounts}.compareTo("income") == 0 ? " AND account_type.code IN ('expense','income')" : "")
+ ($P{limit_to_accounts}.compareTo("balance_sheet") == 0 ? " AND account_type.code NOT IN ('expense','income')" : "")
+ ($P{general_account_ids} == null || $P{general_account_ids}.isEmpty() ? "": (" AND accounts.id IN " + $P{general_account_ids}))
+" ORDER BY accounts.class, accounts.code"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[$P!{query}]]>
	</queryString>
	<field name="devcompany" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="companyname" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="pmin" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="pmax" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="account_account_code" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="account_account_name" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="res_currency_code" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="debit" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="credit" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="solde" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="soldedevise" class="java.lang.Double">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="debitdev" class="java.math.BigDecimal"/>
	<field name="creditdev" class="java.math.BigDecimal"/>
	<field name="account_account_class" class="java.lang.String"/>
	<sortField name="account_account_code"/>
	<variable name="solde_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{solde}]]></variableExpression>
	</variable>
	<variable name="soldedevise_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{soldedevise}]]></variableExpression>
	</variable>
	<variable name="debit_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<variable name="credit_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<variable name="solde_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{solde}]]></variableExpression>
	</variable>
	<variable name="credit_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<variable name="debit_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Classe sous total" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<group name="Classe sous total">
		<groupExpression><![CDATA[$F{account_account_class}]]></groupExpression>
		<groupFooter>
			<band height="35">
				<printWhenExpression><![CDATA[new java.lang.Boolean($P{subtotal}.equals( "1" ))]]></printWhenExpression>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="be1a600e-3083-42c5-bc5e-d67407ad22cb" x="703" y="3" width="83" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{solde_2}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="b1e5e505-1fc4-43b3-b6fd-e79c5a0be1c8" x="623" y="3" width="79" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{credit_2}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="979dd29d-d17c-468a-be47-4d28a74b6ff5" x="542" y="3" width="80" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{debit_2}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="8535c028-b977-4bc6-8c08-8721fd8e726d" x="52" y="3" width="136" height="20"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_account_class}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="fd6d0220-e38b-4eaf-92f1-deb83e0c0962" x="1" y="3" width="50" height="20"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<text><![CDATA[Total Classe]]></text>
				</staticText>
				<line>
					<reportElement uuid="bff89ce2-5917-4f8a-a447-a9210d3ffe25" stretchType="RelativeToBandHeight" x="262" y="0" width="1" height="35"/>
				</line>
				<line>
					<reportElement uuid="e607be37-49ab-4b18-aa7c-af1782cd7d44" stretchType="RelativeToBandHeight" x="295" y="0" width="1" height="35"/>
				</line>
				<line>
					<reportElement uuid="68e94fb3-241c-4fc4-abe7-80feb593f8d5" stretchType="RelativeToBandHeight" x="541" y="0" width="1" height="35"/>
				</line>
				<line>
					<reportElement uuid="8f18d4ad-14fe-4162-bcba-e09aa0f4cf0f" x="376" y="0" width="1" height="35"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="7fb329d2-f209-4d5d-bffd-7e4cbff203ee" x="456" y="0" width="1" height="35"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="16fb7aee-8ca2-4823-aa69-ca245d0da2d0" x="622" y="0" width="1" height="35"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="c8fd8ef3-01b8-42bf-a192-1043db12deac" x="702" y="0" width="1" height="35"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="2b77fa77-6dd9-4d2a-aa92-5074d6938611" x="0" y="0" width="786" height="1"/>
				</line>
				<line>
					<reportElement uuid="a2539d6b-4e67-4ade-8ea8-7da108f7a824" stretchType="RelativeToBandHeight" x="50" y="1" width="1" height="34"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="74">
			<staticText>
				<reportElement uuid="79655e47-d628-4942-90c0-828b0d7377bf" x="0" y="0" width="391" height="32" forecolor="#990000"/>
				<textElement>
					<font fontName="Gisha" size="20" isBold="false"/>
				</textElement>
				<text><![CDATA[Balance des comptes avec devises]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="683e8be8-60f3-4432-8286-e35c4d948385" x="6" y="36" width="50" height="26"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12"/>
				</textElement>
				<text><![CDATA[Société :]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement uuid="da89adde-aacf-4e77-a41e-30fab4093289" x="56" y="36" width="270" height="26" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="bfb976a4-e6dc-4433-b44a-06cace3b2b2b" x="374" y="36" width="354" height="26"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA["Période de "+$F{pmin}+ " à " +$F{pmax}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="90b8067d-e91b-48cd-bf45-5aff9336587c" x="0" y="32" width="786" height="1"/>
			</line>
		</band>
	</title>
	<pageHeader>
		<band height="20">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} !=1]]></printWhenExpression>
			<line>
				<reportElement uuid="355a092d-1940-42b3-8ad5-16eda2daca80" positionType="FixRelativeToBottom" x="0" y="15" width="786" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement uuid="b7b31c0b-4c60-46f3-a27b-aea96913aea6" x="0" y="0" width="424" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Balance des comptes de " + $F{pmin} + " à " + $F{pmax}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="8f5e4d43-1037-4a37-808f-9c95a4858c2e" x="437" y="0" width="348" height="13" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{companyname}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="46">
			<staticText>
				<reportElement uuid="189a99b2-eb81-45e8-b786-dfe855c879de" mode="Opaque" x="0" y="15" width="50" height="20" backcolor="#E1E1E1"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[N° compte]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="345cab82-4f45-4ed1-b2b5-327d6dc22270" mode="Opaque" x="51" y="15" width="210" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Libellé]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f21c7adb-d543-4a46-b6a6-e41050906c3c" mode="Opaque" x="261" y="15" width="35" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Devise]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="21bc557e-640d-4a0b-9c37-c53e3918c5b1" mode="Opaque" x="376" y="15" width="80" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Crédit]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="800e36c0-87b0-4a12-ba28-33e2e93541af" mode="Opaque" x="542" y="15" width="80" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Debit]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f93033fa-52d6-4c72-a039-9392ce12ae02" mode="Opaque" x="622" y="15" width="80" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Crédit]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="be790c22-2d62-420a-8803-6127262363a2" mode="Opaque" x="702" y="15" width="84" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Solde]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b09fe95e-bb0b-4c66-aaa7-8e6c61c11d7a" mode="Opaque" x="456" y="15" width="85" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Solde]]></text>
			</staticText>
			<line>
				<reportElement uuid="d9d1a4cf-bf64-46a5-986b-e9da28a2ab97" x="50" y="15" width="1" height="31"/>
			</line>
			<line>
				<reportElement uuid="dc48655b-f93d-4425-aa2f-b9bedce1f5ce" x="541" y="15" width="1" height="31"/>
			</line>
			<staticText>
				<reportElement uuid="62438ec1-9e9f-4271-a5cd-35773bb8f4a2" mode="Transparent" x="296" y="0" width="245" height="15" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.0" lineStyle="Solid"/>
					<rightPen lineWidth="0.0" lineStyle="Solid"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Devise des opérations]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="00afa3a3-5be1-482d-8f1c-162581e3083f" mode="Opaque" x="296" y="15" width="80" height="20" backcolor="#E1E1E1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<text><![CDATA[Débit]]></text>
			</staticText>
			<line>
				<reportElement uuid="52cc1d9c-f355-4132-b2db-05296c8ec375" x="262" y="15" width="1" height="31"/>
			</line>
			<line>
				<reportElement uuid="8d634152-8a6d-4a37-a972-e276ee4bf3f8" x="295" y="15" width="1" height="31"/>
			</line>
			<line>
				<reportElement uuid="09b093bd-e589-47d5-8154-d68529146f92" x="295" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement uuid="6f465270-dd13-4fa3-948d-bcb7a8a35f42" x="376" y="15" width="1" height="31"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="282751d8-d8b1-4426-ab93-c02545a07048" x="456" y="15" width="1" height="31"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="fb6474b6-57c5-4b51-9923-8088defa77a0" x="541" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement uuid="f8e57e8a-fa03-446a-aaac-048e08198b8e" x="622" y="15" width="1" height="31"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="c16a0d81-c73f-4da5-8766-5e4d5ea9cc39" x="702" y="15" width="1" height="31"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement uuid="03efe5d2-c8b6-442a-8565-92c4738ef8b8" x="542" y="0" width="243" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Gisha" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Devise de base: " + $F{devcompany}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Prevent">
			<frame>
				<reportElement uuid="1223d616-ae13-41a2-a89f-ff4eaf3dd467" key="FrameDetails" style="AlternatingDetailRowStyle" stretchType="RelativeToTallestObject" x="0" y="0" width="786" height="15"/>
				<textField>
					<reportElement uuid="68651f8c-18c0-4813-85fe-4095192f6f77" x="0" y="0" width="50" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_account_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement uuid="4cf0ee8f-4448-4bf8-8eb0-e9d688c74d7c" x="52" y="0" width="210" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_account_name}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="6ef401e7-56f4-48b3-af2a-f11502277a54" x="262" y="0" width="34" height="15">
						<printWhenExpression><![CDATA[$F{devcompany} !=$F{res_currency_code}]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{res_currency_code}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement uuid="4920bbcc-f4d1-4790-9d95-6dc3073e5884" x="703" y="0" width="83" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{solde}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement uuid="7cf9aedc-0aff-4f33-990a-047c7f6aa13c" x="458" y="0" width="82" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code} ) != 0]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{soldedevise}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="79514930-8757-4db3-b09f-6d6d2d7b71dc" x="542" y="0" width="80" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="743944fa-10c6-4a63-8b88-94494112ac8d" x="623" y="0" width="79" height="15"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="e2e8fcc9-3e86-4d25-8f2d-34f653650705" x="296" y="0" width="78" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code} ) != 0]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{debitdev}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;-#,##0.00">
					<reportElement uuid="a4594587-7122-4549-b146-b3636257b37b" x="377" y="0" width="78" height="15">
						<printWhenExpression><![CDATA[$F{devcompany}.compareTo( $F{res_currency_code} ) != 0]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Gisha" size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{creditdev}]]></textFieldExpression>
				</textField>
			</frame>
			<line>
				<reportElement uuid="ddf2c372-c466-4d60-a63f-0cf347b64274" stretchType="RelativeToBandHeight" x="622" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="426b0455-4591-4cdc-9092-210ebfd5097c" stretchType="RelativeToBandHeight" x="702" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="a71e68f1-5c90-4163-b1cc-6691128cf438" stretchType="RelativeToBandHeight" x="456" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="416ef056-7908-4080-a102-aa4b6cbccee5" stretchType="RelativeToBandHeight" x="50" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement uuid="fbc195c7-acff-4a80-bad0-e646c2e97f1c" stretchType="RelativeToBandHeight" x="262" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement uuid="40ca8f7b-ee97-44db-8cc7-1807d4371b37" stretchType="RelativeToBandHeight" x="295" y="0" width="1" height="15"/>
			</line>
			<line>
				<reportElement uuid="2e3f63b0-7f95-4b4d-a2e8-297b4d3c8c33" stretchType="RelativeToBandHeight" x="376" y="0" width="1" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="17d3aac8-e49c-49d0-adab-2853632300e7" stretchType="RelativeToBandHeight" x="541" y="0" width="1" height="15"/>
			</line>
		</band>
	</detail>
	<pageFooter>
		<band height="20">
			<textField>
				<reportElement uuid="05727886-0444-49e7-abaa-a97c95fe4b43" x="617" y="3" width="128" height="13"/>
				<textElement textAlignment="Right">
					<font fontName="Gisha" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="99a704c6-1a70-4192-9e4b-bc3e5d6d6a1e" x="746" y="3" width="40" height="13"/>
				<textElement>
					<font fontName="Gisha" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[" de " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement uuid="bd65f334-d520-4831-b6df-9873db73f745" x="0" y="4" width="190" height="13"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Gisha" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Date d'édition: " + (new SimpleDateFormat("E dd MMMM yyyy")).format(new java.util.Date())]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="fad7d2aa-95ca-4dca-8117-60d92080c060" x="0" y="2" width="786" height="1" forecolor="#999999"/>
				<graphicElement>
					<pen lineStyle="Double" lineColor="#CCCCFF"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band height="144">
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement uuid="4e0031bc-f1b8-4187-a625-a229ce78f8f3" x="702" y="10" width="83" height="28"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{solde_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement uuid="c7f7ae63-cb94-43c3-bc6e-b40d39e44885" x="623" y="10" width="79" height="28"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{credit_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement uuid="989c6c4c-17c2-4c20-b6aa-c6fda25e2480" x="541" y="10" width="81" height="28"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{debit_1}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="c3eee884-2929-4974-85c2-49c32d1249ec" x="0" y="3" width="785" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="00522a70-c282-4a6a-8e67-68f95eed41fc" x="441" y="10" width="98" height="28"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Gisha" size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[Totaux:]]></text>
			</staticText>
			<line>
				<reportElement uuid="e0a627f8-55f7-4c00-8256-b0c938c09cdc" x="0" y="6" width="785" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="6b620d25-8ba1-4d1a-a90d-14fb47b5fd43" x="622" y="10" width="1" height="28"/>
			</line>
			<line>
				<reportElement uuid="39b9f646-e9cc-486d-8eb5-57bbb5b65379" x="702" y="10" width="1" height="28"/>
			</line>
			<subreport>
				<reportElement uuid="c124a938-58eb-49c5-808c-c78588149e14" x="0" y="66" width="786" height="78"/>
				<subreportParameter name="company_id">
					<subreportParameterExpression><![CDATA[$P{company_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="period_ids">
					<subreportParameterExpression><![CDATA[$P{period_ids}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="limit_to_accounts">
					<subreportParameterExpression><![CDATA[$P{limit_to_accounts}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="period_start_id">
					<subreportParameterExpression><![CDATA[$P{period_start_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="period_stop_id">
					<subreportParameterExpression><![CDATA[$P{period_stop_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="general_account_ids">
					<subreportParameterExpression><![CDATA[$P{general_account_ids}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="analytic_account_ids">
					<subreportParameterExpression><![CDATA[$P{analytic_account_ids}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA["repo:/Reports/ab_w_currency_details_subreport"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
